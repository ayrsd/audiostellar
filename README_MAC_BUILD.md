# INSTRUCTIONS FOR MAC BUILD

## Compiling

1. **Clone repo** in of 0.11.x folder /apps/myApps *(advanced users can set OF_ROOT enviroment variable to point where you want instead)*
2. Run **install_addons.sh** from terminal.
3. Double click **AudioStellar.xcodeproj** to open the project in Xcode
4. **Compile and run.** Have a look [here](https://openframeworks.cc/setup/xcode/) if you run into trouble.

## Distribution

1. Open **Disk Utility.app**, go to **File > New Image > Blank Image**. Set Name to **AudioStellar**, Size to **200 MB** and Image Format to **read/write disk image**. Save the image as **temp.dmg**
2. Go to the root of your hard drive, right click **Applications folder** and select **Make Ailas**. This should create another folder named **Applications alias**. Move it to de Desktop and rename it to **Applications**
3. Double click **temp.dmg** to mount it. Drag and drop to the mounted image the compiled **AudioStellar.app** and the **Applications** alias created in the previous step.
4. Right click or ctrl+click > **Show View Options** to customise the look of the image file. *You can select a background image with an arrow to instruct users to drag the .app to the Applications folder in macOS.*
5. **Code Sign** the package:
    1. Open **Keychain Access.app**
    2. Go to **Keychain Access > Certificate Assistant > Create a Certificate...**
    3. Set Name to **AudioStellar**, Identity Type to **Self Signed Root** and Certificate Type to **Code Signing**. Click Create.
    4. Open a terminal and run:
        > sudo codesign -fs AudioStellar /Volumes/AudioStellar/AudioStellar.app
6. In terminal navigate to the folder containing **temp.dmg** and run:
     > hdiutil detach /Volumes/AudioStellar
     
     > hdiutil resize -quiet -sectors min temp.dmg
     
     >hdiutil convert temp.dmg -format UDZO -o AudioStellar_vX.XX.X.dmg
     
7. Delete **temp.dmg** 
8. Package AudioStellar_vX.XX.X.dmg is ready to distribute
