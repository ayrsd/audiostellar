#!/bin/bash
rm -R build/ dist/ audiostellar-onset-detection.spec

if [[ "$OSTYPE" == "darwin"* ]]; then
pyinstaller --onedir --name audiostellar-onset-detection --additional-hooks-dir=. --add-binary $(dirname $(which python))/../lib/python3.6/site-packages/_soundfile_data/libsndfile_x86_64.dylib:_soundfile_data onset-detection.py &&
cp -r dist/audiostellar-onset-detection ../../bin/ &&
printf "\n\nBinary has been built and copied to ../bin/ successfully.\n\n"

else
pyinstaller --onefile --name audiostellar-onset-detection --additional-hooks-dir=. onset-detection.py &&
cp dist/audiostellar-onset-detection ../../bin/ &&
printf "\n\nBinary has been built and copied to ../bin/ successfully.\n\n"

fi
