#!/bin/bash
rm -R build/ dist/ audiostellar-data-analysis.spec

if [[ "$OSTYPE" == "darwin"* ]]; then
pyinstaller --onedir --name audiostellar-data-analysis --additional-hooks-dir=. --add-binary $(dirname $(which python))/../lib/python3.6/site-packages/_soundfile_data/libsndfile_x86_64.dylib:_soundfile_data doProcess.py &&
cp -r dist/audiostellar-data-analysis ../bin/ &&
printf "\n\nBinary has been built and copied to ../bin/ successfully.\n\n"

else
pyinstaller --onefile --name audiostellar-data-analysis --additional-hooks-dir=. doProcess.py &&
cp dist/audiostellar-data-analysis ../bin/ &&
printf "\n\nBinary has been built and copied to ../bin/ successfully.\n\n"

fi
