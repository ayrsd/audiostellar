#include "AudioDimensionalityReduction.h"
string AudioDimensionalityReduction::currentProcess = "";

float AudioDimensionalityReduction::progress = 0;
int AudioDimensionalityReduction::filesFound = 0;
string AudioDimensionalityReduction::generatedJSONPath = "";
vector<string> AudioDimensionalityReduction::failFiles;

string AudioDimensionalityReduction::dirPath = "";

int AudioDimensionalityReduction::targetSampleRate = 22050;
float AudioDimensionalityReduction::targetDuration = 1.3f;
int AudioDimensionalityReduction::stft_windowSize = 2048;
int AudioDimensionalityReduction::stft_hopSize = 2048/4;


string AudioDimensionalityReduction::viz_method = "tsne";
int AudioDimensionalityReduction::tsne_perplexity = 30;
int AudioDimensionalityReduction::tsne_learning_rate = 200;
int AudioDimensionalityReduction::tsne_iterations = 1000;
int AudioDimensionalityReduction::umap_n_neighbors = 15;
float AudioDimensionalityReduction::umap_min_dist = 0.6f;
string AudioDimensionalityReduction::metric = "cosine";

bool AudioDimensionalityReduction::stft = true;
bool AudioDimensionalityReduction::mfcc = false;
bool AudioDimensionalityReduction::spectralCentroid = false;
bool AudioDimensionalityReduction::chromagram = false;
bool AudioDimensionalityReduction::rms = false;

bool AudioDimensionalityReduction::pca_results = true;
bool AudioDimensionalityReduction::force_full_process = false;

string AudioDimensionalityReduction::FILENAME_PROCESS_INFO = "processInfo.json";

void AudioDimensionalityReduction::run() {
    string dataAnalysisFilename = "audiostellar-data-analysis";
	
    #ifdef TARGET_WIN32
    dataAnalysisFilename = "audiostellar-data-analysis.exe";
    #endif

    #ifdef TARGET_LINUX
    FILENAME_PROCESS_INFO = ofFilePath::getUserHomeDir() + "/.config/processInfo.json";
    #endif

    if (dirPath.size() > 0) {
        ofFilePath ofp;
        string binPath;
        
        #ifdef TARGET_LINUX
        binPath = "\"" + ofp.join( ofp.getCurrentExeDir(), dataAnalysisFilename) + "\"";
        #endif
        
        #ifdef TARGET_OS_MAC
        binPath = "\"" + ofp.join( ofp.getCurrentWorkingDirectory() + "/audiostellar-data-analysis", dataAnalysisFilename) + "\"";
        #endif
        
		#ifdef TARGET_WIN32
		binPath = dataAnalysisFilename;
		#endif
        
        string finalDirPath = "\"" + dirPath + "\"";

        string command = binPath + " "
                         + finalDirPath + " "

                         + "--audio-max-length " + ofToString(targetDuration) + " "
                         + "--win-length " + ofToString(stft_windowSize) + " "
                         + "--hop-length " + ofToString(stft_hopSize) + " "

                         + "--viz-method " + viz_method + " "
                         + "--tsne-perplexity " + ofToString(tsne_perplexity) + " "
                         + "--tsne-learning-rate " + ofToString(tsne_learning_rate) + " "
                         + "--tsne-n-iter " + ofToString(tsne_iterations) + " "
                         + "--umap-n-neighbors " + ofToString(umap_n_neighbors) + " "
                         + "--umap-min-dist " + ofToString(umap_min_dist) + " "
                         + "--metric " + ofToString(metric) + " ";

        if (stft) {
            command += "--stft ";
        }
        if (mfcc) {
            command += "--mfcc ";
        }
        if (spectralCentroid) {
            command += "--spectral-centroid ";
        }
        if (chromagram) {
            command += "--chromagram ";
        }
        if (rms) {
            command += "--rms ";
        }

        if (pca_results) {
            command += "--save-pca-results ";
        }
        if (force_full_process) {
            command += "--force-full-process ";
        }

        #ifdef TARGET_LINUX
        command += "--process-status-path \"" + FILENAME_PROCESS_INFO + "\"" ;
        #endif

        ofLog() << "running " << command;
        ofLog() << ofSystem( command );
    }
}

void AudioDimensionalityReduction::checkProcess()
{
    ofxJSONElement file;

    ofFilePath ofp;
    string processInfoPath;
    
    #ifdef TARGET_OS_MAC
    processInfoPath = ofp.join( ofp.getCurrentExeDir() + "../Resources", FILENAME_PROCESS_INFO );
    #endif

    #ifdef TARGET_WIN32
    processInfoPath = ofp.join( ofp.getCurrentExeDir(), FILENAME_PROCESS_INFO);
    #endif

    #ifdef TARGET_LINUX
    processInfoPath = FILENAME_PROCESS_INFO;
    #endif

    ofFile fileCheck(processInfoPath);

    if ( fileCheck.isFile() && file.open( processInfoPath ) ) {
        Json::Value jsonQtyFiles = file["qtyFiles"];
        Json::Value jsonProgress = file["progress"];
        Json::Value jsonCurrentProcess = file["currentProcess"];
        Json::Value jsonGeneratedJSONPath = file["generatedJSONPath"];
        Json::Value jsonFailFiles = file["failFiles"];

        if ( jsonQtyFiles != Json::nullValue ) {
            filesFound = jsonQtyFiles.asInt();
        }
        if ( jsonProgress != Json::nullValue ) {
            progress = jsonProgress.asFloat();
        }
        if ( jsonCurrentProcess != Json::nullValue ) {
            currentProcess = jsonCurrentProcess.asString() + "...";
        }
        if ( jsonGeneratedJSONPath != Json::nullValue ) {
            generatedJSONPath = jsonGeneratedJSONPath.asString();
        }
        if ( jsonFailFiles != Json::nullValue ) {
            failFiles.clear();
            for ( unsigned int i = 0 ; i < jsonFailFiles.size() ; i++ ) {
                failFiles.push_back( jsonFailFiles[i].asString() );
            }
        }

    } else {
        currentProcess = "Initializing...";
    }
}

void AudioDimensionalityReduction::end()
{
    ofFilePath ofp;
    string processInfoPath;
    
    #ifdef TARGET_OS_MAC
    processInfoPath = ofp.join( ofp.getCurrentExeDir() + "../Resources", FILENAME_PROCESS_INFO );
    #endif

    #ifdef TARGET_WIN32
    processInfoPath = ofp.join( ofp.getCurrentExeDir(), FILENAME_PROCESS_INFO);
    #endif

    #ifdef TARGET_LINUX
    processInfoPath = FILENAME_PROCESS_INFO;
    #endif

    ofFile::removeFile( processInfoPath );

    currentProcess = "";
    progress = 0;
    filesFound = 0;
    generatedJSONPath = "";
    failFiles.clear();
}

void AudioDimensionalityReduction::threadedFunction()
{
    if ( isThreadRunning() ) {
        run();
    }
}
