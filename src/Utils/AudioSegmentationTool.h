#pragma once
#include "ofMain.h"
#include "ofxJSON.h"
#include "ofxPDSP.h"
#include "../Sound/VoiceSimple.h"

class AudioSegmentationTool : public ofThread
{
public:
    AudioSegmentationTool();
    static AudioSegmentationTool* getInstance();

    void init();
    void run();
    bool isRunning();
    void loadAudioFile(string path);
    void doSlice();
    void saveSlices();
    void end();

    void checkProcess();
    bool isProcessing = false;

    void threadedFunction();

    string dirPath = "";
    string resultPath = "";
    
    string statusState = "";
    int statusSliced = 0;
    int statusCountMarkers = 0;

    float window_max = 0.03f;
    float window_avg = 0.1f;
    float delta = 0.07f;
    bool backtrack = true;
    float sensitivity = 1-delta;
    
    string audioFilePath = "";
    
    int fade = 1000;
    bool normalize = true;
    
    int time = 500;
    bool save = true;
    
    vector<double> markers = {};
    bool reset_zoom = false;
    bool markers_loaded = false;
    bool needToSort = false;
    int marker_played = 0;
    
    string folder = "";

    string processInfoPath = "";
    
    string FILENAME_PROCESS_ONSETS = "onsets-interprocess.json";

    VoiceSimple player;
    
    pdsp::SampleBuffer * sample = nullptr;

    vector <float> plot_indexes;

    void onSoundCardInit( int & deviceID );
    
    int getDivider(int size, int plotLimitsSize);
    
    int hoveredMarker = -1;
    void getHoveredMarker();
    
    double newMarkerPosition = 0;
    
private:
    static AudioSegmentationTool* instance; 
};

