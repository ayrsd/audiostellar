#include "AudioSegmentationTool.h"

AudioSegmentationTool* AudioSegmentationTool::instance = nullptr;

AudioSegmentationTool::AudioSegmentationTool() : ofThread()
{
    ofAddListener( AudioEngine::getInstance()->soundCardInit, this, &AudioSegmentationTool::onSoundCardInit );
}

AudioSegmentationTool *AudioSegmentationTool::getInstance()
{
    if ( instance == nullptr ) {
        instance = new AudioSegmentationTool();
    }
    
    return instance;
}

void AudioSegmentationTool::init()
{
    #ifdef TARGET_LINUX
    FILENAME_PROCESS_ONSETS = ofFilePath::getUserHomeDir() + "/.config/onsets-interprocess.json";
    #endif
    
    #ifdef TARGET_OS_MAC
    FILENAME_PROCESS_ONSETS = ofFilePath::getCurrentWorkingDirectory() + "/onsets-interprocess.json";
    #endif

    #ifdef TARGET_WIN32
        FILENAME_PROCESS_ONSETS = ofFilePath::getCurrentWorkingDirectory() + "/onsets-interprocess.json";
    #endif
    
    end();
}

void AudioSegmentationTool::run()
{
    string segmentationToolFilename = "audiostellar-onset-detection";

    #ifdef TARGET_WIN32
    segmentationToolFilename = "audiostellar-onset-detection.exe";
    #endif

    ofFilePath ofp;
    string binPath;
        
    #ifdef TARGET_LINUX
    binPath = "\"" + ofp.join( ofp.getCurrentExeDir(), segmentationToolFilename) + "\"";
    #endif
    
    #ifdef TARGET_OS_MAC
    binPath = "\"" + ofp.join( ofp.getCurrentWorkingDirectory() + "/audiostellar-data-analysis", segmentationToolFilename) + "\"";
    #endif

    #ifdef TARGET_WIN32
    binPath = segmentationToolFilename;
    #endif

    string command = binPath;

    #ifdef TARGET_LINUX
    //habria que agregar al script de python que si se le pasa el parametro
    //--process-status-path entonces que use ese path
    command += " --process-status-path \"" + FILENAME_PROCESS_ONSETS + "\"" ;
    #endif

    ofxJSONElement data;
    data["state"] = "loading";
    data.save(FILENAME_PROCESS_ONSETS);
    
    ofLog() << "running " << command;
    ofLog() << ofSystem( command );
}

bool AudioSegmentationTool::isRunning()
{
//    ofFile f(FILENAME_PROCESS_ONSETS);
//    return f.exists();
    
    string result;
    #ifdef TARGET_WIN32
    //tasklist has maximum characters so its audiostellar-onset-detect instead
    //of audiostellar-onset-detection
    result = ofSystem("tasklist | findstr audiostellar-onset-detect");
    #else
    result = ofSystem("pgrep -f audiostellar-onset-detection | grep -v $$");
    #endif
    
    return !result.empty();
    
}

void AudioSegmentationTool::loadAudioFile(string path) {
    
    if (sample == nullptr) sample = new pdsp::SampleBuffer();
    
    sample->load(path);
    
    // plot X axis values
    if (sample->loaded()) {
        plot_indexes.clear();
        for (int i = 0; i < sample->length; i++) {
            plot_indexes.push_back(i);
        }
    }
}

void AudioSegmentationTool::doSlice()
{
    markers.clear();
    markers_loaded = false;
    isProcessing = true;

    ofxJSONElement data;
    data.open(FILENAME_PROCESS_ONSETS);
    data["state"] = "onsetCut";
    data["audioFilePath"] = dirPath;

    data["params"] = Json::objectValue;
    data["params"]["window_max"] = window_max;
    data["params"]["window_avg"] = window_avg;
    data["params"]["delta"] = delta;
    data["params"]["backtrack"] = backtrack;

    data.save(FILENAME_PROCESS_ONSETS);
}

void AudioSegmentationTool::saveSlices()
{
    ofxJSONElement data;
    data.open(FILENAME_PROCESS_ONSETS);
    data["state"] = "saveSlices";
    data["audioFilePath"] = dirPath;
    data["folder"] = resultPath;
    data["save"] = Json::objectValue;
    data["save"]["fade"] = fade;
    data["save"]["normalize"] = normalize;
    
    data["results"] = Json::arrayValue;
    for (int i = 1; i < markers.size() - 1; i++) { //skip first and last marker
        data["results"].append( static_cast<int>(markers[i]) );
    }

    data.save(FILENAME_PROCESS_ONSETS);
    isProcessing = true;
}

void AudioSegmentationTool::threadedFunction()
{
    if ( isThreadRunning() ) {
        run();
    }
}

void AudioSegmentationTool::onSoundCardInit(int &deviceID)
{
    player.patch();
    
    player.out("0") >> player.audioEngine->getAudioOut(0);
    player.out("1") >> player.audioEngine->getAudioOut(1);
    
    player.setVolume(1.0f);
}

int AudioSegmentationTool::getDivider(int bufferLength, int plotLimitsSize) {
    
    if ( bufferLength / plotLimitsSize > 1 ) {
        return 2 * getDivider(bufferLength / 2, plotLimitsSize);
    }
    else {
        return 1;
    }
}

void AudioSegmentationTool::getHoveredMarker() {
    
    // workaround to detect hovered markers
    for (int i = 0; i < markers.size(); i++) {
        ImPlotPoint p = ImPlot::PixelsToPlot(ofGetMouseX(), ofGetMouseY());
        float hoverSize = (ImPlot::GetPlotLimits().Size().x / ImPlot::GetPlotSize().x) ;
        if (p.x > markers[i] - hoverSize * 4 && p.x < markers[i] + hoverSize * 5) {
            hoveredMarker = i;
            return;
        }
    }
    hoveredMarker = -1;
}

void AudioSegmentationTool::end() {
    ofxJSONElement data;
    data.open(FILENAME_PROCESS_ONSETS);
    data["state"] = "end";
    data.save(FILENAME_PROCESS_ONSETS);

    if (isRunning()) {
        #ifdef TARGET_WIN32
        ofSystem("taskkill /IM \"audiostellar-onset-detection.exe\" /F");
        #else
        ofSystem("pkill -f audiostellar-onset-detection");
        #endif
    }
}

void AudioSegmentationTool::checkProcess()
{
    ofxJSONElement data;
    if ( data.open(FILENAME_PROCESS_ONSETS) ) {
        statusState = data["state"].asString();

        // Done finding onsets -> load them
        if (statusState == "done") {
            isProcessing = false;

            if (!markers_loaded) {
                Json::Value onsets = data["results"][0];
                markers.push_back(0); //marker on first sample
                for (int i = 0; i < onsets.size(); i++) {
                    double onset = onsets[i].asDouble();
                    if ( onset == 0 || onset == sample->length - 1) continue;
                    markers.push_back(onset);
                }
                markers.push_back(sample->length - 1); //marker on last sample
                markers_loaded = true;
            }
        }

        // Done cutting files
        if (statusState == "doneAndSave") {
            isProcessing = false;
        }
        
        // Done cutting files
        if (statusState == "errorLoadingFile") {
            isProcessing = false;
        }
        

        if (statusState == "slicingPreprocessing" ||
            statusState == "slicingProcessing") {

            statusSliced = data["sliced"].asInt();
            statusCountMarkers = data["countMarkers"].asInt();
        }
    }

}

