#include "SessionManager.h"
#include "../GUI/CamZoomAndPan.h"
#include "../Servers/AbletonLinkServer.h"

SessionManager* SessionManager::instance = nullptr;

SessionManager::SessionManager(){   
#ifdef TARGET_LINUX
    SETTINGS_FILENAME = ofFilePath::getUserHomeDir() + "/.config/" + SETTINGS_FILENAME;
#endif

#ifdef TARGET_OS_MAC
    SETTINGS_FILENAME = ofFilePath::getUserHomeDir() + "/Library/Application Support/AudioStellar/" + SETTINGS_FILENAME;
#endif

#ifdef TARGET_WIN32
    SETTINGS_FILENAME = ofToDataPath(SETTINGS_FILENAME);
#endif

    units = Units::getInstance();
    sounds = Sounds::getInstance();
    midiServer = MidiServer::getInstance();
    audioEngine = AudioEngine::getInstance();
}

void SessionManager::loadInitSession()
{
    if ( lastCrashed ) {
        lastCrashed = false;
        return;
    }

    //Load last recent project
    if(areAnyRecentProjects(recentProjects)){
        datasetLoadOptions opts;
        opts.method = "RECENT";
        opts.path = recentProjects[recentProjects.size() - 1];

        if ( ofFile::doesFileExist(opts.path, false) ) {
            loadSession(opts);
        }
    }

    if ( !getSessionLoaded() ) {
        loadDefaultSession();
    }
}

void SessionManager::loadSettings(){
    if(!settingsFileExist()){
      createSettingsFile();
    }

    //LOAD FILE
    ofxJSONElement file;
    bool loaded = file.open(SETTINGS_FILENAME);

    //VALIDATION
    if (!loaded) {
        ofLogNotice("ERROR",
                    "Settings could not be loaded");

        ofFile::removeFile(SETTINGS_FILENAME);
        createSettingsFile();

    } else {
        settingsFile = file;
    }

    //SET SETTINGS ACCORDING TO FILE

    //recentProjects
    recentProjects = loadRecentProjects();

    ///////////////
    /// MIDI
    ///////////////

    //set lastMidiDevice (deprecated)
    if(settingsFile["lastMidiDevice"] != Json::nullValue) {
        vector<string> devices;
        devices.push_back( settingsFile["lastMidiDevice"].asString() );
        midiServer->connectDevicesIn(devices);
    }
    if(settingsFile["midiDevicesIn"].size() > 0) {
        vector<string> devices;

        for ( unsigned int i = 0 ; i < settingsFile["midiDevicesIn"].size() ; i++ ) {
            devices.push_back( settingsFile["midiDevicesIn"][i].asString() );
        }

        midiServer->connectDevicesIn(devices);
    }
    if(settingsFile["midiDevicesOut"].size() > 0) {
        vector<string> devices;

        for ( unsigned int i = 0 ; i < settingsFile["midiDevicesOut"].size() ; i++ ) {
            devices.push_back( settingsFile["midiDevicesOut"][i].asString() );
        }

        midiServer->connectDevicesOut(devices);
    }
    if(settingsFile["midiControllerTemplateFilePath"] != Json::nullValue) {
        midiServer->loadControllerTemplateFile(
                    settingsFile["midiControllerTemplateFilePath"].asString() );
    }

    bool midiClock = settingsFile["midiClock"].asBool();
    midiServer->useMIDIClock = midiClock;
    if ( midiClock ) {
        MasterClock::getInstance()->setClockType(MasterClock::MIDI);
    }
    
    bool midiPlayStop = settingsFile["midiPlayStop"].asBool();
    midiServer->handleMIDIPlayStop = midiClock;


    //Show sound filenames
    bool showFilenames = settingsFile["showSoundFilenames"].asBool();
    sounds->setShowSoundFilenames(showFilenames);

    //OSC
    if ( settingsFile["OSC_receivePort"].asInt() > 0 ) {
        OscServer::setReceivePort( settingsFile["OSC_receivePort"].asInt() );
    }
    if ( settingsFile["OSC_sendPort"].asInt() > 0 ) {
        OscServer::setSendPort( settingsFile["OSC_sendPort"].asInt() );
    }
    if ( settingsFile["OSC_sendHost"].asString() != "" ) {
        OscServer::setSendHost( settingsFile["OSC_sendHost"].asString() );
    }
    if ( settingsFile["OSC_enable"].asBool() ) {
        OscServer::getInstance()->start();
    }

    AbletonLinkServer::getInstance()->load(settingsFile["abletonLink"]);
    
    Gui::getInstance()->setShowStats( settingsFile["show_stats"].asBool() );

    if ( !settingsFile["focus_on_clusters"].isNull() ) {
        Gui::getInstance()->setFocusOnClusters(settingsFile["focus_on_clusters"].asBool());
    }
    
    if ( settingsFile["colorPalettes"] != Json::nullValue ) {
        ColorPalette::getInstance()->load(settingsFile["colorPalettes"]);
    }
    else {
        Json::Value colorPalettes = Json::arrayValue;
        colorPalettes.append( ColorPalette::getInstance()->getDefaultPalette() );
        settingsFile["colorPalettes"] = colorPalettes;
        ColorPalette::getInstance()->load(settingsFile["colorPalettes"]);
    }


    //Crash check
    if ( settingsFile["crashCheck"] != Json::nullValue ) {
        if ( settingsFile["crashCheck"].asBool() == true ) {
            audioEngine->load(settingsFile, false);
            Gui::getInstance()->showCrashMessage();
            lastCrashed = true;
            return;
        }
    }

    if ( settingsFile["fullscreen"] != Json::nullValue ) {
        if ( settingsFile["fullscreen"].asBool() == true ) {
            Gui::getInstance()->setFullscreen( true );
        }
    }

    if ( settingsFile["show_gui"] != Json::nullValue ) {
        Gui::getInstance()->setShowGUI( settingsFile["show_gui"].asBool() );
    }

    if ( settingsFile["show_oscmidi_monitors"] != Json::nullValue ) {
        Gui::getInstance()->setShowMIDIOSCMonitor( settingsFile["show_oscmidi_monitors"].asBool() );
    }

    if ( settingsFile["doPreloading"] != Json::nullValue ) {
        Sounds::getInstance()->doPreloading = settingsFile["doPreloading"].asBool();
    }
}

void SessionManager::loadSession(datasetLoadOptions opts){
    string datasetPath;
    ofFile ex;
    //OPENING
    if(opts.method == "GUI"){
        datasetPath = userSelectJson();
        if(datasetPath == "CANCELLED"){
            return;
        }
    } else if(opts.method == "TERMINAL" ||
             opts.method == "RECENT"){
        datasetPath = opts.path;
    }

    isDefaultSession = opts.path == ofToDataPath(DEFAULT_SESSION, true);

    //VALIDATION
    datasetResult * loadingDatasetResult = validateDataset(datasetPath);

    //SHOW ERROR IN CASE THERE IS ONE
    if(!loadingDatasetResult->success) {
        Gui::getInstance()->showMessage(
                    loadingDatasetResult->status.c_str(),
                    STRING_ERROR.c_str()
                    );
    } else {
        //SUCCESSFULL LOADING
        AudioEngine::getInstance()->stop();

        if ( sessionLoaded == true ) {
            Units::getInstance()->reset();
            Sounds::getInstance()->reset();
            MasterClock::getInstance()->play();
            sessionLoaded = false;
        }

        sessionLoaded = true;
        ofxJSONElement &file = loadingDatasetResult->data;
        string audioFilesPath = file["audioFilesPath"].asString();

        if ( audioFilesPath == DEFAULT_SESSION_PLACEHOLDER ) {
           audioFilesPath = ofToDataPath( DEFAULT_SESSION_DIRECTORY, true );
        }

        //Safety measure with paths
        file["audioFilesPathAbsolute"] = Utils::resolvePath
                                    (datasetPath,
                                     audioFilesPath);

        jsonFile = &file;
        jsonFilename = datasetPath;

        if (!isDefaultSession) {
           saveToRecentProject(datasetPath);
        }

        sessionAudioDeviceName = file["audioDeviceName"].asString();
        
        bool askUserForAudioFilesPath = false;

        //existe audioFilesPathAbsolute? si no, preguntar
        if (!ex.doesFileExist(file["audioFilesPathAbsolute"].asString(), false)) {
             file["audioFilesPathAbsolute"] = ofFilePath::removeTrailingSlash(file["audioFilesPathAbsolute"].asString());
             file["audioFilesPathAbsolute"] = ofFilePath::getBaseName(file["audioFilesPathAbsolute"].asString());
             file["audioFilesPathAbsolute"] = Utils::resolvePath
                                          (datasetPath,
                                           file["audioFilesPathAbsolute"].asString());
             if (!ex.doesFileExist(file["audioFilesPathAbsolute"].asString(), false)) {
                 askUserForAudioFilesPath = true;
             }
        }

        if ( !askUserForAudioFilesPath ) {
            finishLoadingSession();
        } else {
            Gui::getInstance()->showFilesNotFoundMessage();
        }
    }

}

string SessionManager::userSelectJson(){
    string defaultPath = "";
    if ( !isDefaultSession && !jsonFilename.empty() ) {
        ofFile file(jsonFilename);
        defaultPath = file.getEnclosingDirectory();
    }

    string filename;
    ofFileDialogResult selection = Gui::getInstance()->showLoadDialog("Select a sound map file (json)",
                                                                      false,
                                                                      defaultPath);

    if(selection.bSuccess){
        filename = selection.getPath();
    } else {
        filename = "CANCELLED";
    }
    return filename;
}

SessionManager::datasetResult * SessionManager::validateDataset(string datasetPath){
   datasetResult * res = new datasetResult;
   string extension = ofFilePath::getFileExt(datasetPath);

   //Exist?
   if ( !ofFile::doesFileExist(datasetPath,false) ) {
       res->success = false;
       res->status = "Sound map file does not exist, it may has been moved or deleted";
       return res;
   }

   //Check extension
   if(extension != "json"){
       res->success = false;
       res->status = "Selected file is not a sound map file";
       return res;
   }

   //Check if valid json
   bool successLoading = res->data.open(datasetPath);
   if(!successLoading){
      res->success = false;
      res->status = "Invalid or corrupted sound map file";
      return res;
   }

   //Check if minimum fields exist
   Json::Value soundFilesPath = res->data["audioFilesPath"];
   Json::Value map = res->data["tsv"];

   if(soundFilesPath == Json::nullValue || map == Json::nullValue){
       res->success = false;
       res->status = "Invalid sound map file.";
       return res;
   }

   res->success = true;
   res->status = "OK";
   return res;

}


void SessionManager::loadDefaultSession(){
    if ( ofFile::doesFileExist( ofToDataPath( DEFAULT_SESSION, true ) ) ) {
        datasetLoadOptions opts;
        opts.method = "RECENT";
        opts.path = ofToDataPath(DEFAULT_SESSION, true);
        loadSession(opts);

        if ( firstTimeOpened ) {
            Gui::getInstance()->showWelcomeScreen();
        }
    } else {
        Gui::getInstance()->showMessage(
                    "Default session not found at " +
                    ofToDataPath( DEFAULT_SESSION, true ) +
                    ".\n\n"
                    "If you've compiled from source, make sure its inside bin/data/",
                    "Warning" );
    }

}


void SessionManager::saveSettingsFile(bool crashCheck){
    Json::Value root = Json::objectValue;

    //Recent Projects
    Json::Value recent = Json::arrayValue;
    for(unsigned int i = 0 ; i < recentProjects.size(); i++){
       recent[i] = recentProjects[i];
    }

    root["recentProjects"] = recent;

    //MIDI devices
    root["midiDevicesIn"] = Json::arrayValue;
    vector<string> midiDevices = midiServer->getConnectedInputDevices();
    for ( auto & m : midiDevices ) {
        root["midiDevicesIn"].append(m);
    }
    root["midiClock"] = midiServer->useMIDIClock;
    root["midiPlayStop"] = midiServer->handleMIDIPlayStop;
    
    
    // deprecated, keeping this for compatibility
    root["lastMidiDevice"] = Json::nullValue;
    root["midiDevicesOut"] = Json::arrayValue;
    midiDevices = midiServer->getConnectedOutputDevices();
    for ( auto & m : midiDevices ) {
        root["midiDevicesOut"].append(m);
    }
    root["midiControllerTemplateFilePath"] = midiServer->getControllerTemplatePath();

    //Ableton link
    root["abletonLink"] = AbletonLinkServer::getInstance()->save();

    //Use original positions
    int useOrigPos = sounds->getUseOriginalPositions() ? 1 : 0;
    root["useOriginalPositions"] = useOrigPos;

    //Show sound filenames
    int showSoundFilenames = sounds->getShowSoundFilenames() ? 1 : 0;
    root["showSoundFilenames"] = showSoundFilenames;

    //OSC
    root["OSC_enable"] = OscServer::enable;
    root["OSC_receivePort"] = OscServer::getReceivePort();
    root["OSC_sendPort"] = OscServer::getSendPort();
    root["OSC_sendHost"] = OscServer::getSendHost();

    root["show_stats"] = Gui::getInstance()->getShowStats();

    root["fullscreen"] = Gui::getInstance()->getFullscreen();
    root["show_gui"] = Gui::getInstance()->getShowGUI();
    root["show_oscmidi_monitors"] = Gui::getInstance()->getShowMIDIOSCMonitor();
    root["focus_on_clusters"] = Gui::getInstance()->getFocusOnClusters();

    root["crashCheck"] = crashCheck;

    //Audio settings
    root["audioSettings"] = audioEngine->save();

    root["doPreloading"] = Sounds::getInstance()->doPreloading;
    //    root["numVoices"] = voices->numVoices;
    
    root["colorPalettes"] = ColorPalette::getInstance()->save();


    settingsFile = root;
    settingsFile.save(SETTINGS_FILENAME, true);
}

bool SessionManager::settingsFileExist(){
  return ofFile::doesFileExist(SETTINGS_FILENAME);
}

void SessionManager::createSettingsFile(){
  Json::Value value = Json::objectValue;

  value["recentProjects"] = Json::arrayValue;
  value["midiDevices"] = Json::arrayValue;
  value["useOriginalPositions"] = false;//Json::Value(Json::booleanValue);
  value["showSoundFilenames"] = true; //Json::Value(Json::booleanValue);

  value["OSC_enable"] = false;//Json::Value(Json::booleanValue);
  value["OSC_receivePort"] = 0;
  value["OSC_sendPort"] = 0;
  value["OSC_sendHost"] = "";
    
  value["audioSettings"] = Json::objectValue;
  value["numVoices"] = 0;

  value["show_stats"] = false;
    
  Json::Value colorPalettes = Json::arrayValue;
  colorPalettes.append( ColorPalette::getInstance()->getDefaultPalette() );
  value["colorPalettes"] = colorPalettes;

  settingsFile = value;
  settingsFile.save(SETTINGS_FILENAME, true);

  firstTimeOpened = true;
}

vector<string> SessionManager::loadRecentProjects(){
    vector<string> r;
    Json::Value recent = Json::Value(Json::arrayValue);

    recent = settingsFile["recentProjects"];
    for(unsigned int i = 0; i < recent.size(); i ++){
      if(recent[i] != Json::nullValue){
        r.push_back(recent[i].asString());
      }
    }

    return r;

}

void SessionManager::saveToRecentProject(string path){
    //Si no hay recientes
    if(recentProjects.size() == 0){
        recentProjects.push_back(path);
    } else {

        for(unsigned int i = 0 ; i < recentProjects.size(); i++){
            if(path == recentProjects[i]){
                recentProjects.erase( recentProjects.begin()+i );
                break;
            }
        }

        recentProjects.push_back(path);
    }

    if ( recentProjects.size() > RECENT_FILES_MAX ) {
        recentProjects.erase( recentProjects.begin() );
    }
}


void SessionManager::saveSession() {
    ofLog() << "Saving...";

    if ( jsonFile == nullptr ) {
        jsonFile = new ofxJSONElement();
    }

    (*jsonFile)["audioDeviceName"] = AudioEngine::getInstance()->getSelectedDeviceName();

    (*jsonFile)["sounds"] = sounds->save();
    (*jsonFile)["units"] = units->save();
    (*jsonFile)["midiMapping"] = midiServer->save();
    (*jsonFile)["oscMapping"] = OscServer::getInstance()->save();

    (*jsonFile)["global"] = Json::objectValue;
    (*jsonFile)["global"]["masterVolume"] = units->masterGain.get();
    (*jsonFile)["global"]["BPM"] = MasterClock::getInstance()->getBPM();
    (*jsonFile)["global"]["cam_translation_x"] = CamZoomAndPan::getInstance()->getTranslation().x;
    (*jsonFile)["global"]["cam_translation_y"] = CamZoomAndPan::getInstance()->getTranslation().y;
    (*jsonFile)["global"]["cam_scale"] = CamZoomAndPan::getInstance()->getScale();

    // I don't want to save the absolute path, I remove it and then add again
    Json::Value audioFilesPathAbsolute = (*jsonFile)["audioFilesPathAbsolute"];
    jsonFile->removeMember("audioFilesPathAbsolute");

    (*jsonFile)["audioFilesPath"] = audioFilesPathAbsolute;
    
    (*jsonFile)["colorPalette"] = colorPalette;

    jsonFile->save(jsonFilename, true);

    (*jsonFile)["audioFilesPathAbsolute"] = audioFilesPathAbsolute;

    ofLog() << "Done saving";
    Gui::getInstance()->showMessage("Session saved successfully", "Information", true);
}

void SessionManager::startAudioWithCurrentConfiguration()
{
    audioEngine->load(settingsFile);
    saveSettingsFile(true);
}

void SessionManager::resetSettings()
{
    createSettingsFile();
    loadSettings();
}

void SessionManager::setAudioFilesNotFoundPath(string path)
{
    ofxJSONElement &file = *jsonFile;

    file["audioFilesPathAbsolute"] = path;
    file["audioFilesPathAbsolute"] = Utils::resolvePath
                              (jsonFilename,
                               file["audioFilesPathAbsolute"].asString());

    finishLoadingSession();
}

void SessionManager::saveAsNewSession(){
    
    #ifdef TARGET_OS_MAC
    string defaultName = "new_sound_map.json";
    #else
    string defaultName = ofFilePath::getEnclosingDirectory(jsonFilename) + "/new_sound_map.json";
    #endif
    ofFileDialogResult result = Gui::getInstance()->showSaveDialog(defaultName, "Save");

    if(result.bSuccess){
        string path = result.getPath();

        if(!ofIsStringInString(path, ".json")){
            path = path + ".json";
        }

        jsonFilename = path;

        if ( isDefaultSession ) {
            (*jsonFile)["audioFilesPath"] = DEFAULT_SESSION_PLACEHOLDER;
        } else {
            (*jsonFile)["audioFilesPath"] = (*jsonFile)["audioFilesPathAbsolute"];
        }

        isDefaultSession = false;
        saveSession();
        setWindowTitleWithSessionName();
        saveToRecentProject(jsonFilename);
    }
}

vector <string> SessionManager::getRecentProjects(){
        vector<string> invertedRecent;
        for(int i = recentProjects.size() - 1; i >= 0; i --){
                invertedRecent.push_back(recentProjects[i]);
        }
        return invertedRecent;
}

ofxJSONElement SessionManager::getAudioSettings()
{
    return settingsFile["audioSettings"];
}

bool SessionManager::getLastCrashed()
{
    return lastCrashed;
}

ofxJSONElement& SessionManager::getSettingsFile() {
    return settingsFile;
}

ofxJSONElement* SessionManager::getSessionFile() {
    return jsonFile;
}

string SessionManager::getSessionFileName() {
    return jsonFilename;
}

bool SessionManager::getSessionLoaded(){
    return sessionLoaded;
}

bool SessionManager::areAnyRecentProjects(vector<string> recentProjects){
   return recentProjects.size() > 0 &&
           recentProjects[recentProjects.size() - 1] != "";
}

void SessionManager::exit(){
   saveSettingsFile(false);
}

void SessionManager::setWindowTitleWithSessionName(){
    ofSetWindowTitle("AudioStellar v" + ofToString(VERSION) + " // " +
                     ofFilePath::getFileName( jsonFilename ) +
                     " (" + ofToString(sounds->getSounds().size()) + " sounds)" );
}

void SessionManager::finishLoadingSession()
{
    ofxJSONElement file = *jsonFile;
    
    if ( file["colorPalette"] != Json::nullValue ) {
        if ( ColorPalette::getInstance()->paletteExists(file["colorPalette"].asString()) ) {
            colorPalette = file["colorPalette"].asString();
        }
        else {
            colorPalette = DEFAULT_PALETTE;
            //show warning, palette not found ?
            //open color settings ?
        }
    }
    else {
        colorPalette = DEFAULT_PALETTE;
    }
    
    //LOAD SOUNDS
    sounds->loadSounds(file);
    sounds->load(file["sounds"]);

    midiServer->reset();
    midiServer->load(file["midiMapping"]);

    OscServer::getInstance()->reset();
    OscServer::getInstance()->load(file["oscMapping"]);

    if ( file["global"] != Json::nullValue ) {
        if ( file["global"]["masterVolume"] != Json::nullValue ) {
            Units::getInstance()->masterVolume.set( file["global"]["masterVolume"].asFloat() );
        }
        if ( file["global"]["BPM"] != Json::nullValue ) {
            MasterClock::getInstance()->setBPM( file["global"]["BPM"].asFloat() );
        }

        if ( file["global"]["cam_translation_x"] != Json::nullValue ) {
            ofVec3f translation;
            translation.x = file["global"]["cam_translation_x"].asFloat();
            translation.y = file["global"]["cam_translation_y"].asFloat();
            translation.z = 0;

            CamZoomAndPan::getInstance()->setTranslation( translation );
            CamZoomAndPan::getInstance()->setScale( file["global"]["cam_scale"].asFloat() );
        }
    }

    //RESET AND LOAD MODES DATA (MIDI MAPPINGS AND SUCH)
    Json::Value jsonUnits = file["units"];
    if ( jsonUnits != Json::nullValue ) {
        units->load(jsonUnits);
    }
    Units::getInstance()->initUnits();

    //SET WIN TITLE
    setWindowTitleWithSessionName();

//    saveSettingsFile(true);
    AudioEngine::getInstance()->start();
}

SessionManager *SessionManager::getInstance()
{
    if ( instance == nullptr ) {
        instance = new SessionManager();
    }

    return instance;
}

void SessionManager::init()
{
    loadSettings();
}

void SessionManager::keyPressed(ofKeyEventArgs & e) {
}
