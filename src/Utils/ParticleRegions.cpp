#include "ParticleRegions.h"

ParticleRegion::ParticleRegion(){
    radius = 0;
    prevY = 0;
    actualY = 0;
    settedCenter = false;
    assigned = false;
    centerPoint = ofVec2f(0,0);
    stepSize = 3;
    r = 0;
}

void ParticleRegion::draw(){
    if(!assigned /*&& MidiServer::midiLearn*/){
        ofNoFill();
        ofSetLineWidth(1);
        ofSetColor(ofColor::white);
        ofDrawCircle(centerPoint, radius);
    }else{
        ofNoFill();
        ofSetLineWidth(3);
        float alpha = ofMap(r, fabs(radius) , fabs(radius) * 1.2, 80, 0);
        ofSetColor(255,0,255,alpha);
        ofDrawCircle(centerPoint, r);
    }
}

void ParticleRegion::update(){

    r+= 0.4;

    if(r >= fabs(radius) * 1.2 ){
        r = fabs(radius);
    }
}

void ParticleRegion::mouseDragged(ofVec2f p){
    if(!settedCenter){
        centerPoint = p;
        settedCenter = true;
    }
    actualY = p.y;
    if(actualY > prevY){
        radius += stepSize;
    }else if(actualY < prevY){
        radius -= stepSize;
    }
    prevY = actualY;
}

void ParticleRegion::mouseReleased(){
    r = fabs(radius);
}

ofVec2f ParticleRegion::getRandomPointInside(){
    ofVec2f p;
    float rad = fabs(radius);
    p.x = ofRandom(centerPoint.x - rad/2, centerPoint.x + rad/2);
    p.y = ofRandom(centerPoint.y - rad/2, centerPoint.y + rad/2);
    return p;
}
bool ParticleRegion::isParticleInsideRegion(ofVec2f p){
    float dist = ofDist(p.x,p.y,centerPoint.x, centerPoint.y);
    return dist <= fabs(radius);
}
//------------------MANAGER--------------------------//
ParticleRegionsManager::ParticleRegionsManager(){
    status = "idle";
}

void ParticleRegionsManager::draw() {
    for(int i = 0; i < areas.size(); i++){
        if (  areas[i] != NULL ) {
            areas[i]->draw();
        }
    }
}

void ParticleRegionsManager::update(){
    for(int i = 0; i< areas.size(); i++){
        if (  areas[i] != NULL ) {
            areas[i]->update();
        }
    }
}

void ParticleRegionsManager::destroy()
{
    if(status == "waiting"){
        destroyLastArea();
        status = "idle";
    }
}

void ParticleRegionsManager::destroyLastArea()
{
    if ( areas.size() > 0 ) {
        areas.erase(areas.end() - 1);
    } else {
        ofLog() << "ERROR: Attempt to delete area when areas is empty";
    }
}

void ParticleRegionsManager::mouseReleased(){
    status = "waiting";
    for(int i = 0; i<areas.size(); i++){
        areas[i]->mouseReleased();
    }
}

void ParticleRegionsManager::mouseDragged(ofVec2f p){
    if(status == "idle"){
        area = NULL;
        area = new ParticleRegion();
        areas.push_back(area);
        status = "drawing";
    }else if(status == "drawing"){
        area->mouseDragged(p);
    }else if(status == "waiting"){
        areas.erase(areas.end() - 1);
        status = "idle";
    }
}

void ParticleRegionsManager::onNoteOn(int note){
    if(status == "waiting"){
       area->note = note;
       area->assigned = true;
       status = "idle";
    }else if(status == "idle"){
    }
}

