#include "ofApp.h"
#include "Sound/Voices.h"
#include "Sound/AudioEngine.h"
//--------------------------------------------------------------
void ofApp::setup() {
    #ifdef TARGET_OS_MAC
        ofSetDataPathRoot("../Resources/data/");
    #endif
    
    ofAddListener(ofEvents().exit, this, &ofApp::tryToExit);

    ofSetWindowTitle("AudioStellar");
    ofSetEscapeQuitsApp(false);

    ofEnableAlphaBlending();
    ofSetVerticalSync(true);
    ofSetFrameRate(60);

    gui = Gui::getInstance();

    sessionManager = SessionManager::getInstance();
    sessionManager->init();

    sounds = Sounds::getInstance();
    midiServer = MidiServer::getInstance();
    oscServer = OscServer::getInstance();
    cam = CamZoomAndPan::getInstance();
    autoPilot = CamAutoPilot::getInstance();
    units = Units::getInstance();
    ast = AudioSegmentationTool::getInstance();

    cam->init();
    
//    Voices::getInstance()->setup();
    MasterClock::getInstance(); //init
//autoPilot->setEnabled(true);
    ast->init();

    bool lastCrashed = sessionManager->getLastCrashed();
    if ( !lastCrashed ) {
        sessionManager->startAudioWithCurrentConfiguration();
    }
    sessionManager->loadInitSession();
}

//--------------------------------------------------------------
void ofApp::update() {
    sounds->update();
    units->update();
    midiServer->update();
    oscServer->update();
    AbletonLinkServer::getInstance()->update();
}

//--------------------------------------------------------------
void ofApp::draw() {
    ofBackground(0);
     autoPilot->update(),

    cam->begin();
        units->beforeDraw();
        sounds->draw();
        units->draw();
    cam->end();

    gui->draw();
}

void ofApp::keyPressed(ofKeyEventArgs & e) {
    gui->keyPressed(e);
 }

void ofApp::keyReleased(ofKeyEventArgs &e)
{
     gui->keyReleased(e);
}

void ofApp::mousePressed(int x, int y, int button) {
    if ( !AudioEngine::getInstance()->isConfigured() ) return;

    ofVec3f realCoordinates = cam->screenToWorld(ofVec3f(x,y,0));
    if(!gui->isMouseHoveringGUI()){
        units->mousePressed( realCoordinates.x,
                             realCoordinates.y,
                             button );
        sounds->mousePressed(realCoordinates.x,
                             realCoordinates.y,
                             false);
    }
    gui->mousePressed(realCoordinates, button);
}

// this won't be called if mouse is also pressed
void ofApp::mouseMoved(int x, int y) {
    if(sessionManager->getSessionLoaded()){
        ofVec3f realCoordinates = cam->screenToWorld(ofVec3f(x,y,0));
        if(!gui->isMouseHoveringGUI()){
            sounds->mouseMoved(realCoordinates.x,
                               realCoordinates.y);
			units->mouseMoved(realCoordinates.x,
				realCoordinates.y);
        } else {
            units->mouseMovedOverGUI(realCoordinates.x,
                realCoordinates.y);
        }
    }
}

void ofApp::mouseDragged(int x, int y, int button) {
    if ( !AudioEngine::getInstance()->isConfigured() ) return;

    if(!gui->isMouseHoveringGUI()){
        ofVec3f realCoordinates = cam->screenToWorld(ofVec3f(x,y,0));

        //order is important
        sounds->mouseDragged(realCoordinates, button);
        units->mouseDragged(realCoordinates.x,
                            realCoordinates.y,
                            button);
    }

}

void ofApp::mouseReleased(int x, int y, int button) {
    if ( !AudioEngine::getInstance()->isConfigured() ) return;

    ofVec3f realCoordinates = cam->screenToWorld(ofVec3f(x,y,0));

    //changing this order will cause sound context menu to be shown after sound moved
    gui->mouseReleased(realCoordinates, button);

    if(!gui->isMouseHoveringGUI()){
        units->mouseReleased(realCoordinates.x,
                             realCoordinates.y,
                             button);
        sounds->mouseReleased(realCoordinates.x,
                             realCoordinates.y,
                             button);
    }
}

void ofApp::mouseScrolled(int x, int y, float scrollX, float scrollY) {
    if ( !AudioEngine::getInstance()->isConfigured() ) return;
    if(!gui->isMouseHoveringGUI()){
        gui->mouseScrolled( ofVec2f(x,y), scrollX, scrollY );
    }
}

void ofApp::exit(){
    if ( gui->getUserWannaExit() ) {
        if (ast->isRunning()) ast->end();
        sessionManager->exit();
        AudioEngine::getInstance()->stop();

        ofLog() << "Exited successfully";
    }
}

bool ofApp::tryToExit(ofEventArgs& args)
{
    /* Make sure to run install.sh for this to work */
    gui->showExitMessage();
    return true;
}



