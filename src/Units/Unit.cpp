#include "Unit.h"

ofEvent<Sound> Unit::eventPlayedSound = ofEvent<Sound>();

Unit::Unit() {
    pan.format = "L | R";
    pan.small = true;

    volume.addListener(this, &Unit::volumeListener);
    volume.set(DEFAULT_VOLUME);
    volume.format = UNIT_VOLUME_FORMAT;

    pan.addListener(this, &Unit::panListener);
    pan.set(0.5f);

    uberMute.addListener(this, &Unit::muteListener);
    uberMute.setColor(uberMute.COLOR_MUTE);
    uberMute.setSmall(true);

    uberSolo.addListener(this, &Unit::soloListener);
    uberSolo.setColor(uberSolo.COLOR_SOLO);
    uberSolo.setSmall(true);

    uberActive.addListener(this, &Unit::activeListener);
    uberActive.setColor(uberActive.COLOR_ACTIVE);
    uberActive.setSmall(true);
    uberActive.set(1.0f);
    
    envEnable.addListener(this, &Unit::envEnableListener);
    envEnable.setSmall(true);
    envEnable.set(0.0f);
    
    envAttack.addListener(this, &Unit::envAttackListener);
    envHold.addListener(this, &Unit::envHoldListener);
    envRelease.addListener(this, &Unit::envReleaseListener);
    
    envAttack.format = "%.0f%%";
    envHold.format = "%.0f%%";
    envRelease.format = "%.0f%%";
    
    sampleReverse.setStyle(UberToggle::CHECKBOX);
    samplePitch.addListener(this, &Unit::samplePitchListener);
    sampleReverse.addListener(this, &Unit::sampleReverseListener);

    parameters.push_back(&volume);
    parameters.push_back(&pan);
    parameters.push_back(&uberMute);
    parameters.push_back(&uberSolo);
    parameters.push_back(&uberActive);
    parameters.push_back(&envAttack);
    parameters.push_back(&envHold);
    parameters.push_back(&envRelease);
    parameters.push_back(&envEnable);
    parameters.push_back(&samplePitch);
    parameters.push_back(&sampleStart);
    parameters.push_back(&sampleReverse);

    parameters.push_back(&panSpread);
    parameters.push_back(&modeSpread);

    selectedOutput.addListener(this, &Unit::selectedOutputListener);

    outGain.enableSmoothing(50);
    panL.enableSmoothing(50);
    panR.enableSmoothing(50);

    patch();
}

Unit::~Unit() {
    unpatch();

    for ( UnitBehavior * b : behaviors ) {
        b->die();
        delete b;
    }
};

void Unit::playSound(shared_ptr<Sound> s, float volume, float pan, float pitch) {
    if (!isActive()) return;
    if (Sounds::getInstance()->isPreloading() && !s->isSampleLoaded() ) {
        return;
    }

    if ( !s->mute && !s->fileNotFound ) {
        if ( restrictToCluster >= 0 &&
             s->getCluster() != restrictToCluster ) {
            return;
        }

        Voice * v = voices.getVoice(s);
//        if ( v == nullptr ) return;

        v->setVolume(volume);

        v->setPan( pan >= 0.f ? ofClamp(pan,0,1) : calculatePanSpread(s) );
        if ( shouldChangePitch(v) ) {
            v->setPitch(pitch == -9999.f ? samplePitch + calculatePitch() : samplePitch + pitch);
//        } else {
//            v->setPan( pan >= 0.f ? ofClamp(pan,0,1) : calculatePanSpread(s) );
//            v->setPitch(pitch == -9999.f ? samplePitch + calculatePitch() : samplePitch + pitch);
        }

        v->setStart(sampleStart);
                
        if(envEnable.get()) {
            v->setEnvelope(envAttack, envHold, envRelease, envMode); //shouldn't be called before setStart()
        }

        if ( !isMuted() && !isMuteForced() ) {
            ofNotifyEvent(eventPlayedSound, *s, this);
        }
    }
}

void Unit::volumeListener(float &v) {
    if (volume.get() == volume.getMin()) {
        volume.format = "-inf";
    } else {
        volume.format = UNIT_VOLUME_FORMAT;
    }
    updateVolume();
    OscServer::getInstance()->mixerChangedVolume(index, volume.get());
}

void Unit::panListener(float &v) {
    updateVolume();
    OscServer::getInstance()->mixerChangedPan(index, pan.get());
}

void Unit::updateVolume() {
    if ( mute || forceMute || volume.get() == volume.getMin() ) {
        panL.set(0.f);
        panR.set(0.f);
    } else {
        //TODO: Changing pan when pan has not been change doesn't sound like a good idea
        panL.set(ofClamp((1.0f - pan.get())*2, 0.f,1.0f));
        panR.set(ofClamp(pan.get()*2, 0.f,1.0f));
    }
}

void Unit::muteListener(float &v) {
    changeMute( uberMute.isActive() );
    OscServer::getInstance()->mixerChangedMute(index, uberMute.isActive());
}

void Unit::soloListener(float &v) {
    changeSolo( uberSolo.isActive() );
    OscServer::getInstance()->mixerChangedSolo(index, uberSolo.isActive());
}

void Unit::activeListener(float &v)
{
    unitActivationChanged(v == 1.0f);
    voices.setActive(v);

    Units::getInstance()->processSoloMuteActive();

    if (v) {
        1.f >> activeAmpL.in_mod();
        1.f >> activeAmpR.in_mod();
    } else {
        0.f >> activeAmpL.in_mod();
        0.f >> activeAmpR.in_mod();
    }

}

void Unit::envEnableListener(float &v) {
    voices.enableEnvelope(envEnable.get());
}

void Unit::envAttackListener(float &v) {
    if (envMode == 1) {
        if (envAttack + envHold + envRelease > 100) {
            if (envHold == 0) {
                envRelease.set( ofClamp(100 - envAttack, 0, 100) );
            }
            else {
                envHold.set( ofClamp(100 - envAttack - envRelease, 0, 100) );
            }
        }
    }
}

void Unit::envHoldListener(float &v) {
    if (envMode == 1) {
        if (envAttack + envHold + envRelease > 100) {
            if (envRelease == 0) {
                envAttack.set( ofClamp(100 - envHold, 0, 100) );
            }
            else {
                envRelease.set( ofClamp(100 - envAttack - envHold, 0, 100) );
            }
        }
    }
}

void Unit::envReleaseListener(float &v) {
    if (envMode == 1) {
        if (envAttack + envHold + envRelease > 100) {
            if (envHold == 0) {
                envAttack.set( ofClamp(100 - envRelease, 0, 100) );
            }
            else {
                envHold.set( ofClamp(100 - envAttack - envRelease, 0, 100) );
            }
        }
    }
}

void Unit::sampleReverseListener(float &v) {
    voices.enableReverse(sampleReverse.get());
    if(sampleReverse == 1) {
        if(sampleStart.get() == START_MIN) {
            sampleStart.set(START_MAX);
        }
    }
    else {
        if(sampleStart.get() == START_MAX) {
            sampleStart.set(START_MIN);
        }
    }
    
}

void Unit::samplePitchListener(float &v) {
    voices.pitchBend( v - lastPitch );

    lastPitch = v;
}

void Unit::setVolume(float v)
{
    volume.set( ofMap(v, 0, 1, volume.getMin(), volume.getMax(), true) );
    updateVolume();
}

void Unit::setEnvelopeMode(int mode) {
    
    if ( mode == 0 ) {
        envAttack.setMin(TIME_ATTACK_MIN);
        envAttack.setMax(TIME_ATTACK_MAX);
        envAttack.set(TIME_ATTACK_DEFAULT);
        envAttack.setTooltip(&Tooltip::UNIT_ENVELOPE_ATTACK_TIME);
        envAttack.log = true;
        envAttack.format = "%.0fms";
        
        envHold.setMin(TIME_HOLD_MIN);
        envHold.setMax(TIME_HOLD_MAX);
        envHold.set(TIME_HOLD_DEFAULT);
        envHold.setTooltip(&Tooltip::UNIT_ENVELOPE_HOLD_TIME);
        envHold.log =true;
        envHold.format = "%.0fms";
        
        envRelease.setMin(TIME_RELEASE_MIN);
        envRelease.setMax(TIME_RELEASE_MAX);
        envRelease.set(TIME_RELEASE_DEFAULT);
        envRelease.setTooltip(&Tooltip::UNIT_ENVELOPE_RELEASE_TIME);
        envRelease.log = true;
        envRelease.format = "%.0fms";

    } else if ( mode == 1 ) {
        envAttack.setMin(0);
        envAttack.setMax(100);
        envAttack.set(PERCENTAGE_ATTACK_DEFAULT);
        envAttack.setTooltip(&Tooltip::UNIT_ENVELOPE_ATTACK_PERCENTAGE);
        envAttack.log = false;
        envAttack.format = "%.0f%%";
        
        envHold.setMin(0);
        envHold.setMax(100);
        envHold.set(PERCENTAGE_HOLD_DEFAULT);
        envHold.setTooltip(&Tooltip::UNIT_ENVELOPE_HOLD_PERCENTAGE);
        envHold.log = false;
        envHold.format = "%.0f%%";
        
        envRelease.setMin(0);
        envRelease.setMax(100);
        envRelease.set(PERCENTAGE_RELEASE_DEFAULT);
        envRelease.setTooltip(&Tooltip::UNIT_ENVELOPE_RELEASE_PERCENTAGE);
        envRelease.log = false;
        envRelease.format = "%.0f%%";
    }
}

ofRectangle Unit::getBoundingBox()
{
    return boundingBox;
}

void Unit::setBoundingBox(ofRectangle c)
{
    boundingBox = c;
}

bool Unit::isActive()
{
    return uberActive.get() == 1.0f;
}

void Unit::setActive(bool v)
{
    if ( v && !isActive() ) {
        uberActive.setActive(true);
    } else if ( !v && isActive() ) {
        uberActive.setActive(false);
    }
}

void Unit::setVoicesAmount(int newVoiceAmount)
{
    int oldVoiceAmount = getVoicesAmount();
    voices.setVoicesAmount(newVoiceAmount);

    if ( newVoiceAmount > oldVoiceAmount ) {
        patchVoices(oldVoiceAmount);
    }
}

int Unit::getVoicesAmount()
{
    return voices.getVoicesAmount();
}

void Unit::muteSound(shared_ptr<Sound> s)
{
    voices.muteSound(s);
}

void Unit::changeMute(bool mute, bool forced) {
    if ( !forced ) {
        this->mute = mute;
    } else {
        this->forceMute = mute;
    }

    if ( !forced ) {
        Units::getInstance()->processSoloMuteActive();
    }

    updateVolume();
}

void Unit::changeSolo(bool v) {
    solo = v;
    Units::getInstance()->processSoloMuteActive();
}

ofRectangle Unit::getRestrictedClusterBB()
{
    return restrictedClusterBB;
}

void Unit::selectedOutputListener(int &v)
{
    AudioEngine::Output selectedOutputObj = AudioEngine::getInstance()->getEnabledOutput(v);

    if ( selectedOutputObj.label != "error" ) {
        unpatch();
        outChannel0 = selectedOutputObj.channel0;
        outChannel1 = selectedOutputObj.channel1;
        patch();
    } else {
        ofLog() << "Tried to set an audio output for unit that is not enabled from audio settings";
        selectedOutput.set(0);
    }

}

void Unit::patchVUmeter()
{
    outL >> vuRMSL >> AudioEngine::getInstance()->getBlackhole();
    outR >> vuRMSR >> AudioEngine::getInstance()->getBlackhole();
    outL >> vuPeakPreL >> AudioEngine::getInstance()->getBlackhole();
    outR >> vuPeakPreR >> AudioEngine::getInstance()->getBlackhole();
}

void Unit::remove()
{
    unpatch();
    voices.die();
    die();

    for ( auto &p : parameters ) {
        p->removeAllMappings();
    }
    effects.removeMappings();
    //TODO: what about behaviors??
}

void Unit::setIndexesToBehaviors(string indexPrefix)
{
    for ( auto &b : behaviors ) {
        b->setIndexesToParameters(indexPrefix);
    }
}

float Unit::calculatePanSpread(shared_ptr<Sound> s)
{
    if ( panSpread == 0.f ) {
        return 0.5f;
    }

    float centerValue, ret;

    switch( modeSpread.getValueAsIndex() ) {
        case 0: //Relative X
            if ( boundingBox.isZero() ) return 0.5f;

            centerValue = panSpread.get() / 2;
            ret = ofMap( s->getPosition().x,
                               boundingBox.getLeft(), boundingBox.getRight(),
                               -centerValue, centerValue, true);
            break;

        case 1: //Relative Y
            if ( boundingBox.isZero() ) return 0.5f;

            centerValue = panSpread.get() / 2;
            ret = ofMap( s->getPosition().y,
                               boundingBox.getTop(), boundingBox.getBottom(),
                               -centerValue, centerValue, true);
            break;

        case 2: //Absolute X
            centerValue = panSpread.get() / 2;
            ret = ofMap( s->getPosition().x,
                               0, Sounds::getInstance()->getInitialWindowWidth(),
                               -centerValue, centerValue, true);
            break;

        case 3: //Absolute Y
            centerValue = panSpread.get() / 2;
            ret = ofMap( s->getPosition().y,
                               0, Sounds::getInstance()->getInitialWindowHeight(),
                               -centerValue, centerValue, true);
            break;

        case 4: //Random
            ret = ofRandom(-0.5f, 0.5f);
            break;

    }

    return 0.5f + ret;
}

float Unit::calculatePitch()
{
    if ( requestedPitches.empty() ) return 0;

    int randomIdx = (int)ofRandom(requestedPitches.size());
    return requestedPitches[randomIdx];
}

bool Unit::shouldChangePitch( Voice * v )
{
    if ( !v->beingReused ) {
        return true;
    } else {
        if ( v->pitchChangedWhen < requestedPitchesLastChanged ) {
            v->pitchChangedWhen = ofGetElapsedTimeMillis();
            return true;
        }

        return false;
    }
}

void Unit::openMIDIKeyboardEnvelope()
{
    if ( !isMIDIKeyboardEnvelopeOpen ) {
        isMIDIKeyboardEnvelopeOpen = true;
        adsrTrigger.trigger(1.0f);

        if ( !MIDIKeyboardSaveNotes ) {
            requestedPitches.clear();
        }
    }
}

void Unit::repatchOutput()
{
    //AudioEngine::Output selectedOutputObj = AudioEngine::getInstance()->getEnabledOutput(v);

    unpatch();
    //outChannel0 = selectedOutputObj.channel0;
    //outChannel1 = selectedOutputObj.channel1;
    patch();
}

void Unit::setIndex(int i) {
    string name = "Unit" + ofToString(i);
    effects.setIndexesToEffects(name);
    setIndexesToBehaviors(name);
    name = "##" + name;

    for ( unsigned int j = 0 ; j < parameters.size() ; j++ ) {
        parameters[j]->setName(parameters[j]->originalName + name);
    }

    setIndexExtra(i);

    uberActive.setName(ofToString(i+1) + "##Active");

    index = i;
}

int Unit::getIndex()
{
    return index;
}

Json::Value Unit::saveBehaviors() {
    Json::Value root = Json::Value( Json::arrayValue );
    for(unsigned int i = 0; i < behaviors.size(); i++) {
        UnitBehavior * behavior = behaviors[i];
        Json::Value jsonUnit = Json::Value( Json::objectValue );
        jsonUnit["name"] = behavior->getName();
        jsonUnit["params"] = behavior->save();
        root.append(jsonUnit);
    }
    return root;
}

void Unit::loadBehaviors( Json::Value jsonData )
{
    if ( jsonData != Json::nullValue ) {
        for ( UnitBehavior * b : behaviors ) {
            for ( unsigned int i = 0 ; i < jsonData.size() ; i++ ) {
                if ( jsonData[i]["name"] == b->getName() ) {
                    b->load(jsonData[i]["params"]);
                    break;
                }
            }
        }
    }
}

void Unit::setMute(bool v)
{
    uberMute.setActive(v);
}

void Unit::setSolo(bool v)
{
    uberSolo.setActive(v);
}

void Unit::setVoicesType(Voices::VoicesType type)
{
    voices.setVoicesType(type);
}

Voices::VoicesType Unit::getVoicesType()
{
    return voices.getVoicesType();
}

void Unit::setPolyphonyChoke(bool choke)
{
    voices.setChoke(choke);
}

bool Unit::getPolyphonyChoke()
{
    return voices.getChoke();
}

string Unit::getCustomName()
{
    return customName;
}

void Unit::setCustomName(string name)
{
    customName = name;
}

float Unit::getVolume(bool normalized)
{
    float v = volume.get();
    if ( normalized ) {
        v = ofMap(v, volume.getMin(), volume.getMax(), 0, 1, true);
    }
    return v;
}

void Unit::patch(){

    addModuleInput("0", activeAmpL );
    addModuleInput("1", activeAmpR );

    activeAmpL >> effects.in("0");
    activeAmpR >> effects.in("1");

    adsr.set(0,0,1,0,1.0f);

    patchVoices();

//    adsr.setCurve(1.0f);

    adsrTrigger.out_trig() >> adsr.in_trig() >> adsrAmpL.in_mod();
    adsr >> adsrAmpR.in_mod();

    adsrTrigger.trigger(1.0f);

    //outGain is for volume, outL and outR is for panning
    effects.out("0") >> outGain.ch(0) >> outL;
    effects.out("1") >> outGain.ch(1) >> outR;

    panL >> outL.in_mod();
    panR >> outR.in_mod();

    outL >> Units::getInstance()->masterGain.ch( outChannel0 ) ;
    outR >> Units::getInstance()->masterGain.ch( outChannel1 ) ;

    1.f >> activeAmpL.in_mod();
    1.f >> activeAmpR.in_mod();

    updateVolume();
    patchVUmeter();
}

void Unit::patchVoices(int fromVoiceIdx)
{
    for ( unsigned int i = fromVoiceIdx ; i < getVoicesAmount() ; i++ ) {
        Voice * v = voices.voices[i];
        v->out("0") >> adsrAmpL >> in("0");
        v->out("1") >> adsrAmpR >> in("1");
    }
}

void Unit::unpatch() {
    // i don't think this is needed
    for ( auto &v : voices.voices ) {
        v->disconnectOut();
    }

    outL.disconnectAll();
    outR.disconnectAll();
    outGain.disconnectAll();
    vuRMSL.disconnectAll();
    vuRMSR.disconnectAll();
    vuPeakPreL.disconnectAll();
    vuPeakPreR.disconnectAll();
    panL.disconnectAll();
    panR.disconnectAll();
}
