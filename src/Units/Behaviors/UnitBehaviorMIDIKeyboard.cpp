#include "UnitBehaviorMIDIKeyboard.h"
#include "../ParticleUnit.h"

UnitBehaviorMIDIKeyboard::UnitBehaviorMIDIKeyboard(Unit * parentUnit)
    : UnitBehavior(parentUnit)
{
    ofAddListener( MidiServer::getInstance()->midiKeyboardEvent, this, &UnitBehaviorMIDIKeyboard::onMIDIMessage );
    tab = "Keyboard";

    parameters.push_back(&cboMIDIChannel);
    parameters.push_back(&cboRootNote);
\
    parameters.push_back(&envEnable);
    parameters.push_back(&envAttack);
    parameters.push_back(&envDecay);
    parameters.push_back(&envSustain);
    parameters.push_back(&envRelease);

    parameters.push_back(&chkEmitParticles);
    if ( dynamic_cast<ParticleUnit*>(parent) != nullptr) {
        chkEmitParticles.set(1.0f);
        chkEmitParticles.setStyle( UberToggle::CHECKBOX );
    }

    envEnable.setSmall(true);
    envEnable.addListener(this, &UnitBehaviorMIDIKeyboard::enableListener);
    envAttack.addListener(this, &UnitBehaviorMIDIKeyboard::attackListener);
    envDecay.addListener(this, &UnitBehaviorMIDIKeyboard::decayListener);
    envSustain.addListener(this, &UnitBehaviorMIDIKeyboard::sustainListener);
    envRelease.addListener(this, &UnitBehaviorMIDIKeyboard::releaseListener);
    cboMIDIChannel.addListener(this, &UnitBehaviorMIDIKeyboard::MIDIChannelChangedListener);
}

UnitBehaviorMIDIKeyboard::~UnitBehaviorMIDIKeyboard()
{
    ofRemoveListener( MidiServer::getInstance()->midiKeyboardEvent, this, &UnitBehaviorMIDIKeyboard::onMIDIMessage );
}

Json::Value UnitBehaviorMIDIKeyboard::save()
{
    Json::Value midiConfig = Json::Value( Json::objectValue );
        midiConfig["MIDIChannel"] = cboMIDIChannel.get();
        midiConfig["RootNote"] = cboRootNote.get();
        midiConfig["EmitParticles"] = chkEmitParticles.get();

        midiConfig["envEnable"] = envEnable.get();
        midiConfig["Attack"] = envAttack.get();
        midiConfig["Decay"] = envDecay.get();
        midiConfig["Sustain"] = envSustain.get();
        midiConfig["Release"] = envRelease.get();

        midiConfig["SaveNotes"] = parent->MIDIKeyboardSaveNotes;
    return midiConfig;
}

void UnitBehaviorMIDIKeyboard::load(Json::Value jsonData)
{
    if ( jsonData["MIDIChannel"].asFloat() > 0 ) {
        cboMIDIChannel.set( jsonData["MIDIChannel"].asFloat() );
    }
    cboRootNote.set( jsonData["RootNote"].asFloat() );

    if ( jsonData["envEnable"].asBool() ) {
        envEnable.set( true );
    }

    if ( !jsonData["EmitParticles"].isNull() ) {
        chkEmitParticles.set( jsonData["EmitParticles"].asFloat());
    }

    envAttack.set( jsonData["Attack"].asFloat() );
    envDecay.set( jsonData["Decay"].asFloat() );
    envSustain.set( jsonData["Sustain"].asFloat() );
    envRelease.set( jsonData["Release"].asFloat() );

    parent->MIDIKeyboardSaveNotes = jsonData["SaveNotes"].asBool();
}

void UnitBehaviorMIDIKeyboard::onMIDIMessage(MIDIKeyboardNotes &m)
{
    //All o el MIDI channel que es
    if ( cboMIDIChannel.getValueAsIndex() != 1 &&
         m.channel != cboMIDIChannel.getValueAsIndex() - 1 ) return;

    vector<int> pitchs;
    for ( unsigned int i = 0 ; i < 128 ; i++) {
        if ( m.velocities[i] > 0 ) {
            int pitch = i - (60 + cboRootNote.getValueAsIndex()); //60 CENTRAL_C4 MIDI NOTE
            pitchs.push_back(pitch);
        }
    }

    if ( !pitchs.empty() ) {
//        string ps;
//        for ( auto &p : pitchs ) {
//            ps += ofToString(p) + ", ";
//        }
//        ofLog() << ps;
        if ( parent->requestedPitches != pitchs ) {
            parent->requestedPitches = pitchs;
            parent->requestedPitchesLastChanged = ofGetElapsedTimeMillis();
        }
    } else {
        //"note off"
        vector<int> nothing;
        ofNotifyEvent( keyboardEvent, nothing, this );
    }

    // chkEmitParticles will be -1 except for Particle Units
    if ( m.status == MIDI_NOTE_ON && chkEmitParticles.get() != 0.f ) {

        ofNotifyEvent( keyboardEvent, pitchs, this );
    }

    if ( envEnable.get() ) {
        float t = pitchs.empty() ? 0.f : 1.0f;
        if ( t != lastTriggered ) {
            lastTriggered = t;
            if ( t > 0 ) {
                parent->adsrTrigger.trigger(t);
            } else {
                parent->adsrTrigger.off();
            }
        }
        parent->isMIDIKeyboardEnvelopeOpen = false;
    }
}

void UnitBehaviorMIDIKeyboard::drawGui()
{
    cboMIDIChannel.draw();
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::UNIT_BEHAVIOR_MIDI_KEYBOARD_MIDI_CHANNEL);

    if ( parent->isMIDIKeyboardEnvelopeOpen ) {
        envEnable.set(0.f);
    }

    if ( cboMIDIChannel.getValueAsIndex() != 0 ) {
        cboRootNote.draw();
        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::UNIT_BEHAVIOR_MIDI_KEYBOARD_ROOT_NOTE);
        ImGui::NewLine();

        // is Particle Unit
        if ( chkEmitParticles.get() > -1 ) {
            chkEmitParticles.draw();
            ImGui::NewLine();
        }

        bool shouldPop = false;

        if(!envEnable) {
            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.4f);
            shouldPop = true;
        }

        ExtraWidgets::TextDisabled("Envelope");
        ImGui::SameLine(ImGui::GetContentRegionAvail().x - 15);
        envEnable.draw();

        envAttack.draw();
        envDecay.draw();
        envSustain.draw();
        envRelease.draw();

        if(shouldPop) {
            ImGui::PopStyleVar();
        }
    }

    if ( cboMIDIChannel.getValueAsIndex() != 0 || parent->MIDIKeyboardSaveNotes ) {
        if ( ImGui::Checkbox("Save notes", &(parent->MIDIKeyboardSaveNotes) )) {
            if ( !parent->MIDIKeyboardSaveNotes ) {
                parent->requestedPitches.clear();
            }
        }
        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::UNIT_BEHAVIOR_MIDI_KEYBOARD_SAVE_NOTES);
    }
}

void UnitBehaviorMIDIKeyboard::enableListener(float &v)
{
    if ( v == 0 ) {
        parent->openMIDIKeyboardEnvelope();
    } else {
        parent->adsrTrigger.off();
        parent->isMIDIKeyboardEnvelopeOpen = false;
    }
}

void UnitBehaviorMIDIKeyboard::attackListener(float &v)
{
    v * 1000 >> parent->adsr.in_attack();
}

void UnitBehaviorMIDIKeyboard::decayListener(float &v)
{
    v * 1000 >> parent->adsr.in_decay();
}

void UnitBehaviorMIDIKeyboard::sustainListener(float &v)
{
    v >> parent->adsr.in_sustain();
}

void UnitBehaviorMIDIKeyboard::releaseListener(float &v)
{
    v * 1000 >> parent->adsr.in_release();
}

void UnitBehaviorMIDIKeyboard::MIDIChannelChangedListener(float &v)
{
    if ( cboMIDIChannel.getValueAsIndex() == 0 ) {
        parent->openMIDIKeyboardEnvelope();
    }
}
