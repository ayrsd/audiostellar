#pragma once

#include "ofMain.h"
#include "ofxJSON.h"
#include "ofxPDSP.h"

#include "../Utils/Utils.h"
#include "../Utils/Tooltip.h"
#include "../Sound/Sound.h"
#include "../Sound/Voices.h"
#include "../Effects/AudioEffects.h"
#include "../Servers/MidiServer.h"
#include "../GUI/UberSlider.h"
#include "../GUI/UberToggle.h"
#include "../GUI/UberButton.h"
#include "../GUI/UberCombo.h"
#include "Behaviors/UnitBehavior.h"

class UnitBehavior;
class Voices;

class Unit : public pdsp::Patchable {
private:
#define DEFAULT_VOLUME -10.f
#define UNIT_VOLUME_FORMAT "%.2fdB"
#define UNIT_VOLUME_MIN -47.99f
#define UNIT_VOLUME_MAX 12.f

// Envelope
#define TIME_ATTACK_DEFAULT 250.f
#define TIME_ATTACK_MIN 0.f
#define TIME_ATTACK_MAX 5000.f
#define TIME_HOLD_DEFAULT 100.f
#define TIME_HOLD_MIN 0.f
#define TIME_HOLD_MAX 5000.f
#define TIME_RELEASE_DEFAULT 500.f
#define TIME_RELEASE_MIN 0.f
#define TIME_RELEASE_MAX 5000.f
#define PERCENTAGE_ATTACK_DEFAULT 40.f
#define PERCENTAGE_HOLD_DEFAULT 20.f
#define PERCENTAGE_RELEASE_DEFAULT 40.f
#define ENVELOPE_MODE_TIME 0
#define ENVELOPE_MODE_PERCENTAGE 1

// Sample playback
#define PITCH_DEFAULT 0.f
#define PITCH_MIN -24.f
#define PITCH_MAX 24.f
#define START_DEFAULT 0.f
#define START_MIN 0.f
#define START_MAX 0.99f
#define REVERSE_DEFAULT 1.f
#define REVERSE_MIN -1.f
#define REVERSE_MAX 1.f

    pdsp::Amp activeAmpL;
    pdsp::Amp activeAmpR;
    pdsp::AmpFull outL;
    pdsp::AmpFull outR;
    pdsp::ParameterGain outGain;
    UberSlider volume { outGain.set("##volume", DEFAULT_VOLUME, UNIT_VOLUME_MIN, UNIT_VOLUME_MAX),
                        &Tooltip::UNIT_VOLUME };
    pdsp::AmpFull adsrAmpL;
    pdsp::AmpFull adsrAmpR;

    UberSlider pan {"##pan", 0.5f, 0.f, 1.f, &Tooltip::UNIT_PAN};
    pdsp::ValueControl panL;
    pdsp::ValueControl panR;

    bool mute = false; // user chose to mute the track
    bool forceMute = false; // forced by units (i.e other soloed track)
    bool solo = false;
    UberToggle uberMute {"M##ute", &Tooltip::UNIT_MUTE};
    UberToggle uberSolo {"S##olo", &Tooltip::UNIT_SOLO};
    void changeMute(bool mute, bool forced = false);
    void changeSolo(bool v);

    //Inactive units free resources and will be able to be activated/inactivated on tempo
    //Name will change to unit number on setIndex()
    UberToggle uberActive {"A##ctive", &Tooltip::UNIT_ACTIVE};

    //VU meter
    pdsp::RMSDetector vuRMSL;
    pdsp::RMSDetector vuRMSR;
    pdsp::AbsoluteValue vuPeakPreL;
    pdsp::AbsoluteValue vuPeakPreR;
    
    int envMode = ENVELOPE_MODE_PERCENTAGE;
    UberToggle envEnable { " ##envEnable", &Tooltip::UNIT_ENVELOPE_ENABLE };
    UberSlider envAttack { "Attack", PERCENTAGE_ATTACK_DEFAULT, 0.f, 100.f, &Tooltip::UNIT_ENVELOPE_ATTACK_PERCENTAGE };
    UberSlider envHold { "Hold", PERCENTAGE_HOLD_DEFAULT, 0.f, 100.f, &Tooltip::UNIT_ENVELOPE_HOLD_PERCENTAGE };
    UberSlider envRelease { "Release", PERCENTAGE_RELEASE_DEFAULT, 0.f, 100.f, &Tooltip::UNIT_ENVELOPE_RELEASE_PERCENTAGE };
    
    float lastPitch = 0.f;
    UberSlider samplePitch { "Pitch", PITCH_DEFAULT, PITCH_MIN, PITCH_MAX, &Tooltip::UNIT_SAMPLE_PITCH };
    UberSlider sampleStart { "Start", START_DEFAULT, START_MIN, START_MAX, &Tooltip::UNIT_SAMPLE_START };
    UberToggle sampleReverse { "Reverse", &Tooltip::UNIT_SAMPLE_REVERSE };

    UberCombo modeSpread {"Mode##panSpread", {
            "Relative X",
            "Relative Y",
            "Absolute X",
            "Absolute Y",
            "Random"}, &Tooltip::UNIT_SPREAD_MODE};
    UberSlider panSpread {"Pan##panSpread", 0.f, 0.f, 1.f, &Tooltip::UNIT_SPREAD_PAN};

    Voices voices;
    AudioEffects effects;

    //Selected output
    ofParameter<int> selectedOutput = 0;
    void selectedOutputListener(int &v);

    void patchVUmeter();

    int outChannel0 = 0;
    int outChannel1 = 1;

    int restrictToCluster = -1;
    ofRectangle restrictedClusterBB;
    bool restrictToClusterSelecting = false;

    // this is call when removing by units
    // it won't fully delete the object
    void remove();


    friend class Units;
    friend class UnitBehaviorMIDIKeyboard;
    friend class UnitBehaviorDragToPlay;
protected:
    //this is set by units and constructs unique labels
    int index = -1;

    string customName = "";

    //Is unit selected by the user?
    bool selected = false;

    //Some units doesn't need this feature
    bool restrictToClusterEnabled = true;

    ////////////////////
    // Push your UberControls here
    // It will take care of defining unique IDs
    // NOT DOING THIS WILL BREAK OSC/MIDI MAPPING
    ////////////////////
    vector<UberControl *> parameters;

    void setIndexesToBehaviors(string indexPrefix);
    float calculatePanSpread(shared_ptr<Sound> s);

    //All units MUST calculate their bounding box.
    //This is a rectangle where they are active
    //It is needed for pan spread
    ofRectangle boundingBox;

    //MIDI Keyboard
    pdsp::ADSR adsr;
    pdsp::TriggerControl adsrTrigger;
    vector<int> requestedPitches;
    long requestedPitchesLastChanged = 0;

    float calculatePitch();
    bool shouldChangePitch(Voice * v);

    void openMIDIKeyboardEnvelope();
    bool isMIDIKeyboardEnvelopeOpen = true;
    bool MIDIKeyboardSaveNotes = false;

    // This is called by Unit when active is changed
    virtual void unitActivationChanged(bool v) {};
public:
    Unit();
    virtual ~Unit();

    // It is unlikely to need to reimplement these
    virtual void patch();
    virtual void patchVoices(int fromVoiceIdx = 0);
    virtual void unpatch();
    virtual void repatchOutput();

    //All units MUST have a name
    virtual string getUnitName() = 0;

    //All units MUST be able to measure its distance
    //to a given point. It is used for ctrl+clicking
    //Each unit will have its own way of defining this
    virtual float getDistanceFrom(ofVec2f point) {return -1;}

    ////////////////////
    // Methods Unit developers might want to override
    //
    // Declaring these on your .h but not implementing them
    // in your .cpp will result on a cryptic linker error.
    // Just declare AND implement the ones you need
    ////////////////////
    virtual void beforeDraw() {}
    virtual void draw() {}
    virtual void drawGui(){}
    virtual void update() {}
    virtual void reset() {}
    virtual void die() {} //for some reason, destructor won't remove notify from master clock //is this still happening ??

    virtual void mousePressed(ofVec2f p, int button) {}
    virtual void mouseDragged(ofVec2f p, int button) {}
    virtual void mouseReleased(ofVec2f p){}
    virtual void keyPressed(ofKeyEventArgs & e) {}
    virtual void mouseMoved(ofVec2f p){}
    virtual void mouseMovedOverGUI(ofVec2f p){}
    virtual void midiMessage( MIDIMessage m){}

    // These MUST be defined
    virtual Json::Value save() = 0;
    virtual void load( Json::Value jsonData ) = 0;
    //////////////////////////////////////////////////

    // It is unlikely to need to reimplement playSound
    virtual void playSound(shared_ptr<Sound> s, float volume = 1.0f, float pan = -1.f, float pitch = -9999.f);
    static ofEvent<Sound> eventPlayedSound;

    //Behaviors are functional modules shared between units
    vector<UnitBehavior *> behaviors;

    // Mute / solo / volume managment
    void volumeListener(float &v);
    void panListener(float &v);
    void updateVolume();
    void muteListener(float &v);
    void soloListener(float &v);
    void activeListener(float &v);
    void envEnableListener(float &v);
    void envAttackListener(float &v);
    void envHoldListener(float &v);
    void envReleaseListener(float &v);
    void sampleReverseListener(float &v);
    void samplePitchListener(float &v);
    void muteSound( shared_ptr<Sound> s );

    ////////////////////
    // This sets unique IDs to almost every control.
    // All UberControls MUST be pushed to parameters vector,
    // otherwise MIDI/OSC won't work correctly
    //
    // Normally you shouldn't care about this,
    // but if you are using non uber controls
    // Then, implement setIndexExtra. (see SequenceUnit)
    ////////////////////
    void setIndex(int i);
    virtual void setIndexExtra(int i) {}
    int getIndex();

    Json::Value saveBehaviors();
    void loadBehaviors( Json::Value jsonData );

    // When user selects a Unit
    // it should reveal where the unit is on the map
    virtual void onSelectedUnit() { selected = true; }
    virtual void onUnselectedUnit() { selected = false; }

    // Getters and setters
    bool isMuted() { return mute; }
    bool isMuteForced() { return forceMute; }
    bool isSolo() { return solo; }
    bool isSelected() { return selected; }
    void setMute( bool v );
    void setSolo( bool v );
    void setVoicesType( Voices::VoicesType type );
    Voices::VoicesType getVoicesType();
    void setPolyphonyChoke(bool choke);
    bool getPolyphonyChoke();
    string getCustomName();
    void setCustomName(string name);
    float getVolume(bool normalized = true);
    void setVolume(float v);
    void setEnvelopeMode(int mode);
    ofRectangle getBoundingBox();
    void setBoundingBox(ofRectangle c);
    bool isActive();
    void setActive(bool v);
    void setVoicesAmount(int newVoiceAmount);
    int getVoicesAmount();

    ofRectangle getRestrictedClusterBB();
};
