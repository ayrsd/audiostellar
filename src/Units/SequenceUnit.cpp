#include "SequenceUnit.h"
#include "../GUI/ExtraWidgets.h"
#include "Behaviors/UnitBehaviorDragToPlay.h"
#include "Behaviors/UnitBehaviorPolyphony.h"
#include "Behaviors/UnitBehaviorOSC.h"
#include "Behaviors/UnitBehaviorMIDIKeyboard.h"
#include "ofxMidiMessage.h"

int SequenceUnit::intBars[QTY_BARS] = { QTY_STEPS/4,
                                        QTY_STEPS/2,
                                        QTY_STEPS,
                                        QTY_STEPS*2,
                                        QTY_STEPS*3,
                                        QTY_STEPS*4,
                                        QTY_STEPS*8};

SequenceUnit::SequenceUnit() {
    restrictToClusterEnabled = false;

    parameters.push_back(&probPlay);
    parameters.push_back(&probGrow);
    parameters.push_back(&probShrink);
    parameters.push_back(&probMutate);
    parameters.push_back(&probPreventDeath);
    parameters.push_back(&probPreserveClusters);
    parameters.push_back(&btnGrow);
    parameters.push_back(&btnMutate);
    parameters.push_back(&btnShrink);

    parameters.push_back(&cboSelectedBars);
    parameters.push_back(&offset);
    parameters.push_back(&btnClearSequence);

    probPreventDeath.setStyle(UberToggle::CHECKBOX);
    probPreserveClusters.setStyle(UberToggle::CHECKBOX);

    offset.format = "%.0f";
    offset.addListener(this, &SequenceUnit::onNeedToProcess);
    cboSelectedBars.addListener(this, &SequenceUnit::onNeedToProcess);
    btnClearSequence.addListener(this, &SequenceUnit::onClearSequence);
    btnGrow.addListener(this, &SequenceUnit::onBtnGrow);
    btnShrink.addListener(this, &SequenceUnit::onBtnShrink);
    btnMutate.addListener(this, &SequenceUnit::onBtnMutate);

    ofAddListener( MasterClock::getInstance()->onTempo, this, &SequenceUnit::onTempo );

    behaviors.push_back( new UnitBehaviorDragToPlay(this) );
    behaviors.push_back( new UnitBehaviorOSC(this) );
    behaviors.push_back( new UnitBehaviorMIDIKeyboard(this) );

    //set poly settings before creating
    setVoicesType(Voices::VoicesType::FullPolyphonic);
//    setPolyphonyChoke( false );
    behaviors.push_back( new UnitBehaviorPolyphony(this) );
    
    stepDuration = ( (60.0 / MasterClock::getInstance()->getBPM()) / 4.0 ) * 1000;
    
}

void SequenceUnit::onBtnGrow(float &a)
{
    if ( isActive() && a >= 1.f ) {
        forceGrow = true;
    }
}

void SequenceUnit::onBtnShrink(float &a)
{
    if ( isActive() && a >= 1.f ) {
        forceShrink = true;
    }
}

void SequenceUnit::onBtnMutate(float &a)
{
    if ( isActive() && a >= 1.f ) {
        forceMutate = true;
    }
}

void SequenceUnit::grow()
{
    shared_ptr<Sound> newSound = chooseNewSound();
    if ( newSound != nullptr ) {
        toggleSound( newSound );
    }
}

void SequenceUnit::shrink()
{
    if ( !secuencia.empty() &&
         !( probPreventDeath && probPreserveClusters
            && secuencia.size() == selectedClusters.size() ) ) {

        shared_ptr<Sound> rmSound = nullptr;

        if ( probPreventDeath && probPreserveClusters ) {
            //que el random sea solo entre los candidatos
            vector< shared_ptr<Sound> > candidates;
            for ( shared_ptr<Sound> s : secuencia ) {
                if ( selectedClusters[s->getCluster()] > 1 ) {
                    candidates.push_back( s );
                }
            }

            if (candidates.size() > 0) {
                int randomIdx = (int)ofRandom(0, candidates.size());
                rmSound = candidates[randomIdx];
            }


        } else {
            if ( !probPreventDeath || secuencia.size() > 1 ) {
                int randomIdx = (int)ofRandom(0, secuencia.size());
                rmSound = secuencia[randomIdx];
            }
        }

        if (rmSound != nullptr) {
            toggleSound(rmSound);
        }
    }
}

void SequenceUnit::mutate()
{
    shared_ptr<Sound> newSound =
            chooseNewSound( secuenciaEnTiempo[currentStep]->getCluster() );

    if ( newSound != nullptr ) {
        for ( unsigned int i = 0 ; i < secuencia.size() ; i++ ) {
            if ( secuencia[i] == secuenciaEnTiempo[currentStep] ) {
                toggleSound( secuencia[i], false, false );
                toggleSound( newSound, false, false );

                secuencia[i] = newSound;
                break;
            }
        }

        processSequence();
    }
}

shared_ptr<Sound> SequenceUnit::chooseNewSound( int cluster )
{
    shared_ptr<Sound> newSound = nullptr;
    vector<shared_ptr<Sound>> sounds;

    if ( !probPreserveClusters || secuencia.empty() ) {
        sounds = Sounds::getInstance()->getSounds();
    } else {
        if ( cluster != -2 ) { //not noise
            sounds = Sounds::getInstance()->getSoundsByCluster(cluster);
        } else {
            if ( !selectedClusters.empty() ) {
                sounds = Sounds::getInstance()->getSoundsByClusters(selectedClusters);
            }
        }
    }

    if ( !sounds.empty() ) {
        for ( shared_ptr<Sound> s : secuencia ) {
            sounds.erase(std::remove(sounds.begin(), sounds.end(), s), sounds.end());
        }

        if ( sounds.size() > 0 ) {
            do {
                int randomIdx = (int)ofRandom(0, sounds.size());
                newSound = sounds[randomIdx];
            } while ( newSound->mute );
        }

    }

    return newSound; //it can be null !!
}

void SequenceUnit::onNeedToProcess(float &a){
    processSequence();
}

void SequenceUnit::onClearSequence(float &a)
{
    clearSequence();
}

void SequenceUnit::calculateBoundingBox()
{
    if ( !secuencia.empty() ) {
        if ( secuencia.size() > 1 ) {
            ofRectangle boundingBox(
                        secuencia[0]->getPosition().x,
                        secuencia[0]->getPosition().y, 1, 1);


            for ( auto &s : secuencia) {
                boundingBox.growToInclude( s->getPosition() );
            }
            setBoundingBox( boundingBox );
        } else {
            ofRectangle boundingBox;

            boundingBox.setFromCenter(
                        secuencia[0]->getPosition().x,
                        secuencia[0]->getPosition().y,
                        5,
                        5
                        );

            setBoundingBox( boundingBox );
        }
    } else {
        setBoundingBox( ofRectangle() );
    }
}

Json::Value SequenceUnit::save() {
    Json::Value sequence = Json::Value( Json::objectValue );
    Json::Value ids = Json::Value( Json::arrayValue );

    for ( unsigned int j = 0 ; j < secuencia.size() ; j++ ) {
        ids.append( secuencia[j]->id );
    }

    sequence["ids"] = ids;
    sequence["offset"] = offset.get();
    sequence["probPlay"] = probPlay.get();
    sequence["probAdd"] = probGrow.get();
    sequence["probRemove"] = probShrink.get();
    sequence["probChange"] = probMutate.get();
    sequence["bars"] = cboSelectedBars.getValueAsIndex();
    sequence["preventDeath"] = probPreventDeath.get();
    sequence["preserveClusters"] = probPreserveClusters.get();

    return sequence;
}
void SequenceUnit::load(Json::Value jsonData) {
    if ( jsonData["bars"] != Json::nullValue ) cboSelectedBars.set( cboSelectedBars.indexToValue(jsonData["bars"].asInt()) );

    if ( jsonData["ids"] != Json::nullValue ) {
        Json::Value ids = jsonData["ids"];
        for ( unsigned int i = 0 ; i < ids.size() ; i++ ) {
            toggleSound( Sounds::getInstance()->getSoundById( ids[i].asInt() ) , false);
        }
    }

    if ( jsonData["offset"] != Json::nullValue ) offset.set( jsonData["offset"].asInt() );
    if ( jsonData["probPlay"] != Json::nullValue ) probPlay.set( jsonData["probPlay"].asFloat() );
    if ( jsonData["probAdd"] != Json::nullValue ) probGrow.set( jsonData["probAdd"].asFloat() );
    if ( jsonData["probRemove"] != Json::nullValue ) probShrink.set( jsonData["probRemove"].asFloat() );
    if ( jsonData["probChange"] != Json::nullValue ) probMutate.set( jsonData["probChange"].asFloat() );
    if ( jsonData["preventDeath"] != Json::nullValue ) probPreventDeath.set( jsonData["preventDeath"].asFloat() );
    if ( jsonData["preserveClusters"] != Json::nullValue ) probPreserveClusters.set( jsonData["preserveClusters"].asFloat() );    

    processSequence();
    setSelectAllSounds(false);
}

void SequenceUnit::onSelectedUnit() {
    Unit::onSelectedUnit();
    setSelectAllSounds();

    for ( auto &s : secuencia ) {
        Units::getInstance()->revealUnit( s->getPosition() );
    }
}
void SequenceUnit::onUnselectedUnit() {
    Unit::onUnselectedUnit();
    setSelectAllSounds(false);
}

void SequenceUnit::setSelectAllSounds(bool selected)
{
    for ( unsigned int i = 0 ; i < secuencia.size() ; i++ ) {
        secuencia[i]->selected = selected;
    }
}

void SequenceUnit::draw() {
//tss look for beforeDraw
}

void SequenceUnit::drawGui()
{
    if ( ExtraWidgets::Header("Sequence") ) {

        cboSelectedBars.draw();
//        ofxImGui::AddCombo()

        offset.draw();
        probPlay.draw();

        if ( secuencia.size() > 0 ) {
            ImGui::NewLine();
            btnClearSequence.draw();
        }

        ImGui::NewLine();
    }

    if ( ExtraWidgets::Header("Evolution") ) {

        probGrow.draw();
        ImGui::SameLine();
        btnGrow.draw();

        probShrink.draw();
        ImGui::SameLine();
        btnShrink.draw();

        probMutate.draw();
        ImGui::SameLine();
        btnMutate.draw();

        probPreserveClusters.draw();
        probPreventDeath.draw();

        ImGui::NewLine();
    }
}

void SequenceUnit::midiMessage(MIDIMessage m)
{
    // is this ever called ? (not from Bitwig)
    if ( m.status == MIDI_START ) {
        currentStep = -1;
    }
}

float SequenceUnit::getDistanceFrom(ofVec2f point)
{
    float distance = -1.f;
    if ( secuencia.size() > 0 ) {
        if ( secuencia.size() == 1 ) {
            ofVec2f soundPosition = secuencia[0]->getPosition();
            distance = ofDistSquared( point.x,
                                      point.y,
                                      soundPosition.x,
                                      soundPosition.y );
        } else {
            float step = 0.05f;
            ofVec2f pointAtPercent = polyLine.getPointAtPercent(0);
            distance = ofDistSquared( point.x,
                                      point.y,
                                      pointAtPercent.x,
                                      pointAtPercent.y );

            for ( float i = step; i <= 1.f ; i += step ) {
                pointAtPercent = polyLine.getPointAtPercent(i);
                float lineDistance = ofDistSquared( point.x,
                                                    point.y,
                                                    pointAtPercent.x,
                                                    pointAtPercent.y );
                if ( lineDistance < distance ) {
                    distance = lineDistance;
                }
            }
        }
    }

    return distance;
}

void SequenceUnit::onTempo(int & frame)
{
    if ( !isActive() || isProcessing ) return;

    currentStep = frame % getCantSteps();

    if ( forceGrow ) {
        grow();
        forceGrow = false;
    }
    if ( forceShrink ) {
        shrink();
        forceShrink = false;
    }

    if ( secuenciaEnTiempo != nullptr && secuenciaEnTiempo[currentStep] != nullptr ) {
        if ( ofRandom(1.f) < probPlay.get() ) {
            playSound(secuenciaEnTiempo[currentStep]);
        }

        if ( ofRandom(1.f) < probMutate || forceMutate ) {
            mutate();
            forceMutate = false;
        }
    }

    if ( currentStep == 0 ) {
        if ( ofRandom(1.f) < probGrow ) {
            grow();
        }
        if ( ofRandom(1.f) < probShrink ) {
            shrink();
        }
    }
    
    //maybe claculate this when tempo changes
    stepDuration = ( (60.0 / MasterClock::getInstance()->getBPM()) / 4.0 ) * 1000; //time interval between tempo frames in milliseconds
    
    currentStepTime = ofGetElapsedTimeMillis();
        
}

void SequenceUnit::processSequence()
{
    isProcessing = true;
    polyLine.clear();

    secuenciaEnTiempo = new shared_ptr<Sound>[getCantSteps()];
    vector<int> steps;

    for ( int i = 0 ; i < getCantSteps() ; i++ ) {
        secuenciaEnTiempo[i] = nullptr;
    }

    unsigned int secuenciaSize = secuencia.size();

    if ( secuenciaSize == 0 ) {
        calculateBoundingBox();
        isProcessing = false;
        return;
    }  
    secuenciaEnTiempo[0] = secuencia[0];
    if ( secuenciaSize == 1 ) {
        processSequenceOffset();
        calculateBoundingBox();
        isProcessing = false;
        return;
    }

    if ( getCantSteps() < secuencia.size() ) {
        secuenciaSize = getCantSteps();

        // If all steps are taken, then there is not much to calculate.
        // Just put each sound in each step
        for ( unsigned int i = 1 ; i < secuenciaSize ; i++ ) {
            steps.push_back(i);
        }
    } else { // There are more steps than sounds, where does each sound go ?

        // First we build a vector of distances and calculate the length of the whole path
        // Each position of this vector is the distance to ALL previous points.
        vector<float> distancias;
        float ultimaDistancia = -1;
        float distanciaAcumulada = 0;
        for ( unsigned int i = 1 ; i < secuenciaSize ; i++ ) {
            float distancia = secuencia[i-1]->getPosition().distance(secuencia[i]->getPosition());
            distanciaAcumulada += distancia;

            if ( ultimaDistancia != -1 ) {
                distancia += ultimaDistancia;
            }

            distancias.push_back(distancia);
            ultimaDistancia = distancia;
        }

        // We add the distance between the last point and the first one
        float distanciaUltimoConPrimero = secuencia[secuenciaSize-1]->getPosition()
                                            .distance(secuencia[0]->getPosition());
        distanciaAcumulada += distanciaUltimoConPrimero;

        // We map each distance with the step it should belong
        for ( unsigned int i = 0 ; i < distancias.size() ; i++ ) {
            distancias[i] = ofMap( distancias[i],
                                   0, distanciaAcumulada,
                                   1, getCantSteps()-1, true );

            // We discretize the mapping and assert there are no sounds
            // belonging to the same step
            int step = round (distancias[i]);

            while( find( steps.begin(), steps.end(), step ) != steps.end() ) {
                step++;
            }

            steps.push_back(step);
        }

        // This doesn't work right in some cases,
        // We assert that steps are correct, and if not, correct them
        bool hayError = false;
        do {
            hayError = false;
            for ( unsigned int i = 0 ; i < steps.size() ; i++ ) {
                // Assigned step is larger than the steps we actually have ?
                if ( steps[i] > getCantSteps() - 1 ) {
                    hayError = true;
                    steps[i]--;
                    // No problem, just move each sound one step less
                    for ( int j = i-1 ; j >= 0 ; j-- ) {
                        if ( steps[j] == steps[j+1] ) {
                            steps[j]--;
                        } else {
                            break;
                        }
                    }
                    break;
                }
            }
            // Do this until there is no error (hopefully)
        } while( hayError );
    }

    // Now we are ready to build our final vector.
    // It works just like a step sequencer:
    // each position is a fixed figure, sound pointer if there is a sound to play,
    // nullptr if silence.
    for ( unsigned int i = 0 ; i < steps.size() ; i++ ) {
        secuenciaEnTiempo[ steps[i] ] = secuencia[i+1];
    }

    for ( unsigned int i = 0 ; i < secuenciaSize ; i++ ) {
        polyLine.addVertex( secuencia[i]->getPosition().x,
                            secuencia[i]->getPosition().y, 0 );
    }

    polyLine.close();
    
    calculateStepsPercentage();

    // Offset the whole vector if needed
    processSequenceOffset();

    calculateBoundingBox();
    
    isProcessing = false;
}

void SequenceUnit::processSequenceOffset()
{
    shared_ptr<Sound> * secuenciaEnTiempoOffset = new shared_ptr<Sound>[getCantSteps()];

    for ( int i = 0 ; i < getCantSteps() ; i++ ) {
        secuenciaEnTiempoOffset[ (i + int(offset.get()) ) % getCantSteps() ] = secuenciaEnTiempo[i];
    }

    secuenciaEnTiempo = secuenciaEnTiempoOffset;
}

void SequenceUnit::calculateStepsPercentage() {
    
    stepsPercentage.clear();
    
    //get normalized lenght of polyLine at each vertex
    vector<float> lengthsAtIndex;
    for ( int i = 0 ; i < polyLine.getVertices().size(); i++ ) {
        lengthsAtIndex.push_back( polyLine.getLengthAtIndex(i) / polyLine.getPerimeter() );
    }
    lengthsAtIndex.push_back(1.f);
    
    //get normalized lenght of each individual segment of polyLine
    vector<float> lengths;
    for ( int i = 0 ; i < lengthsAtIndex.size() - 1; i++ ) {
        lengths.push_back(lengthsAtIndex[i + 1] - lengthsAtIndex[i]);
    }
    
    //get number of steps between sounds in sequence
    vector<int> steps;
    int count = 1;
    for ( int i = 1; i < getCantSteps() ; i++ ) {
        if(secuenciaEnTiempo[i] == nullptr) {
            count++;
        }
        else {
            steps.push_back(count);
            count = 1;
        }
    }
    steps.push_back(count);
    
    //get each step as a percentage of total sequence lenght
    for ( int i = 0; i < steps.size() ; i++ ) {
        for (int j = 0; j < steps[i]; j++ ) {
            stepsPercentage.push_back(lengths[i] / steps[i]);
        }
    }
}

void SequenceUnit::clearSequence()
{
    setSelectAllSounds(false);
    secuencia.clear();

    if ( secuenciaEnTiempo != nullptr ) {
        for ( int i = 0 ; i < getCantSteps() ; i++ ) {
            secuenciaEnTiempo[i] = nullptr;
        }
    }

    polyLine.clear();
    setBoundingBox( ofRectangle() );
//    reset();
}

int SequenceUnit::getCantSteps()
{
    return intBars[cboSelectedBars.getValueAsIndex()];
}

void SequenceUnit::toggleSound(shared_ptr<Sound> sound, bool doProcessSequence, bool modifySequence)
{
    if ( sound != nullptr ) {
        if ( std::find(secuencia.begin(), secuencia.end(), sound) == secuencia.end() ) {
            if ( secuencia.size() < getCantSteps() || !modifySequence ) {
                if ( modifySequence ) {
                    secuencia.push_back( sound );
                }

                if ( sound->getCluster() != -2 ) {
                    if ( selectedClusters.find( sound->getCluster() )
                         == selectedClusters.end() ) {

                        selectedClusters.insert( pair<int,int>(sound->getCluster(), 1 ) );
                    } else {
                        selectedClusters[sound->getCluster()]++;
                    }
                }

                if ( selected ) {
                    sound->selected = true;
                }
            }
        } else {
            selectedClusters[sound->getCluster()]--;
            if ( selectedClusters[sound->getCluster()] <= 0 ) {
                selectedClusters.erase( sound->getCluster() );
            }

            if ( modifySequence ) {
                secuencia.erase(
                    std::remove(secuencia.begin(),
                    secuencia.end(), sound), secuencia.end());
            }

            sound->selected = false;
        }

        if ( doProcessSequence ) {
            processSequence();
        }
    }
}



void SequenceUnit::beforeDraw() {
    if ( isProcessing ) {
        return;
    }
    
    ofSetLineWidth(2);
    
    if ( selected ) {
        ofSetColor(ofColor(255,255,255,128));
    } else {
        ofSetColor(ofColor(80,80,80,128));
    }
    
    polyLine.draw();
    
    if (isActive()) {
        if ( playing ) {
            ofSetColor(ofColor(255,255,255));
            ofFill();
            
            if ( secuencia.size() > 0 ) {
                ofPoint p;
                
                float subStepTime = (ofGetElapsedTimeMillis() - currentStepTime) / stepDuration;
                float subStepPercentage = (1.0f / getCantSteps()) * subStepTime;
                
                float offsetedStep = ( currentStep + (getCantSteps() - int(offset.get()) ) ) % getCantSteps();
                float percentage = ofClamp( ( offsetedStep / getCantSteps() ) + subStepPercentage, 0.f, 1.f );
                
                p = polyLine.getPointAtPercent(percentage);
                
                if ( !(p.x == 0.0f && p.y == 0.0f)) {
                    ofDrawEllipse( p.x , p.y, 5, 5 );
                }
            }
        }
    }
}

void SequenceUnit::mousePressed(ofVec2f p, int button) {
    if ( button == 0 && !Gui::getInstance()->isAnyModifierPressed() ) {
        shared_ptr<Sound> hoveredSound = Sounds::getInstance()->getHoveredSound();
        if ( hoveredSound != nullptr ) {
            toggleSound( hoveredSound, true );
        }
    }
}

void SequenceUnit::mouseDragged(ofVec2f p, int button) {
    // -1 is sent if "mouse" is actually OSC
    if ( button == -1 ) {
        shared_ptr<Sound> hoveredSound = Sounds::getInstance()->getNearestSound(p);
        if ( hoveredSound != nullptr ) {
            toggleSound( hoveredSound, true );
        }
    }
}

void SequenceUnit::reset()
{
    probPlay.set(1.f);
    offset.set(0);
}

void SequenceUnit::die()
{
    ofRemoveListener( MasterClock::getInstance()->onTempo, this, &SequenceUnit::onTempo );
    clearSequence();
};
