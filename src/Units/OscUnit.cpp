#include "OscUnit.h"
#include "../Sound/AudioEngine.h"
#include "Behaviors/UnitBehaviorPolyphony.h"

void OscUnit::oscPlay(float volume)
{
    oscCursorOpacity = 255;

    shared_ptr<Sound> nearestSound = Sounds::getInstance()->getNearestSound( oscPosition );
    if ( nearestSound != nullptr ) {
        playSound( nearestSound, volume );
    }
}

OscUnit::OscUnit(){
    ofAddListener( OscServer::getInstance()->oscEvent, this, &OscUnit::onOSCMessage );

    //set poly settings before creating
    setVoicesType(Voices::VoicesType::Polyphonic);
    setPolyphonyChoke( false );
    behaviors.push_back( new UnitBehaviorPolyphony(this) );
}

void OscUnit::onOSCMessage(ofxOscMessage &m)
{
    ofLog() << m.getAddress();

    string p = "";
    if ( strncmp(prefix, "", 64) ) {
        p = "/";
        p += string(prefix);
    }

    if(m.getAddress() == p + OSC_PLAY) {
        if ( m.getNumArgs() >= 2 ) {
            float volume = 1.0f;
            float x = m.getArgAsFloat(0);
            float y = m.getArgAsFloat(1);
            ofRectangle bb = getRestrictedClusterBB();

            if ( bb.isZero() ) {
                oscPosition = ofVec2f(x * Sounds::getInstance()->getInitialWindowWidth(),
                                      y * Sounds::getInstance()->getInitialWindowHeight());
            } else {
                oscPosition = ofVec2f(ofMap(x,0,1,bb.getX(),bb.getX()+bb.getWidth()),
                                      ofMap(y,0,1,bb.getY(),bb.getY()+bb.getHeight()));
            }

            if (m.getNumArgs() >= 3 ) {
                volume = m.getArgAsFloat(2);
            }

            oscPlay(volume);
        } else {
            ofLog() << "No arguments for OSC: " << m.getAddress();
        }
    } else if ( m.getAddress() == p + OSC_PLAY_X ) {
        if ( m.getNumArgs() >= 1 ) {
            float volume = 1.0f;
            float x = m.getArgAsFloat(0);
            ofRectangle bb = getRestrictedClusterBB();

            if ( bb.isZero() ) {
                x *= Sounds::getInstance()->getInitialWindowWidth();
            } else {
                x = ofMap(x,0,1,bb.getX(),bb.getX()+bb.getWidth());
            }

            oscPosition = ofVec2f(x, oscPosition.y);

            if (m.getNumArgs() >= 2) {
                volume = m.getArgAsFloat(1);
            }

            oscPlay(volume);
        } else {
            ofLog() << "No arguments for OSC: " << m.getAddress();
        }
    } else if ( m.getAddress() == p + OSC_PLAY_Y ) {
        if ( m.getNumArgs() >= 1 ) {
            float volume = 1.0f;
            float y = m.getArgAsFloat(0);
            ofRectangle bb = getRestrictedClusterBB();

            if ( bb.isZero() ) {
                y *= Sounds::getInstance()->getInitialWindowHeight();
            } else {
                y = ofMap(y,0,1,bb.getY(),bb.getY()+bb.getHeight());
            }

            oscPosition = ofVec2f(oscPosition.x, y);

            if (m.getNumArgs() >= 2) {
                volume = m.getArgAsFloat(1);
            }

            oscPlay(volume);
        }
    } else if ( m.getAddress() == p + OSC_PLAY_ID ) {
        if ( m.getNumArgs() >= 1 ) {

            float volume = 1.0f;

            if ( m.getNumArgs() >= 2 ) {
                volume = m.getArgAsFloat(1);
            }

            shared_ptr<Sound> soundToPlay = Sounds::getInstance()->getSoundById( m.getArgAsInt(0) );
            if ( soundToPlay != NULL ) {
                playSound( soundToPlay, volume );
            } else {
                ofLog() << "ID not found: " << m.getAddress();
            }
        } else {
            ofLog() << "No arguments for OSC: " << m.getAddress();
        }
    } else if ( m.getAddress() == p + OSC_PLAY_CLUSTER_BY_SOUND_ID) {
        if ( m.getNumArgs() >= 1 ) {
            int id = m.getArgAsInt(0);
            shared_ptr<Sound> s = Sounds::getInstance()->getSoundById( id );
            if ( s != NULL ) {
                if ( s->getCluster() > -1 ) {
                    vector<shared_ptr<Sound>> cluster = Sounds::getInstance()->getSoundsByCluster( s->getCluster() );
                    shared_ptr<Sound> soundToPlay = nullptr;

                    if ( m.getNumArgs() >= 2 ) {
                        int soundIdx = m.getArgAsInt(1);
                        soundIdx = soundIdx % cluster.size();
                        soundToPlay = cluster[soundIdx];
                    } else { //if index is not provided choose random
                        do {
                            soundToPlay = cluster[ floor( ofRandom(0, cluster.size()) ) ];
                        } while( soundToPlay == s );
                    }

                    float volume = 1.0f;

                    if ( m.getNumArgs() >= 3 ) {
                        volume = m.getArgAsFloat(2);
                    }

                    playSound( soundToPlay, volume );
                } else {
                    ofLog() << "Sound is not in a cluster for OSC: " << m.getAddress();
                }
            }

        } else {
            ofLog() << "No arguments for OSC: " << m.getAddress();
        }
    }

    else if(m.getAddress() == p + OSC_PLAY_CLUSTER) {
        vector<shared_ptr<Sound>> snds;
        string clusterName = "";
        unsigned int sndIdx = 0;
        float volume = 0.9f;

        if(m.getNumArgs() > 0){
            clusterName = m.getArgAsString(0);
            snds = Sounds::getInstance()->getSoundsByCluster(clusterName, false);

            if ( snds.size() == 0 ) {
                ofLog() << "No sounds found in cluster for OSC: " << m.getAddress();
                return;
            }
        } else {
            ofLog() << "No arguments for OSC: " << m.getAddress();
            return;
        }
        //Sólo estoy mandando el cluster, elijo index randommente
        if(m.getNumArgs() == 1) {
            sndIdx = floor( ofRandom(0, snds.size()));
        }
        //Mando cluster e index
        if(m.getNumArgs() >= 2) {
            sndIdx = m.getArgAsInt(1);
            sndIdx = sndIdx % snds.size();
        }
        //Mando cluster index y volumen
        if(m.getNumArgs() == 3){
           volume = m.getArgAsFloat(2);
        }

        playSound(snds[sndIdx], volume);
    }
}

void OscUnit::update()
{
    if ( oscCursorOpacity > 0 ) oscCursorOpacity -= 2;
}

void OscUnit::draw()
{
    if ( oscCursorOpacity < 1 ) return;

    ofNoFill();
    ofSetColor(255,255,255, oscCursorOpacity);
    ofDrawLine((oscPosition.x), (oscPosition.y - 4.5f), (oscPosition.x), (oscPosition.y + 4.5f));
    ofDrawLine((oscPosition.x - 4.5f), (oscPosition.y), (oscPosition.x + 4.5f), (oscPosition.y));
}

void OscUnit::drawGui()
{
    string label = "OSC Prefix##" + ofToString(index);
    ImGui::InputText(label.c_str(), prefix, 64);
    if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_OSC_PREFIX);

    ImGui::NewLine();
}

void OscUnit::die()
{
    ofRemoveListener( OscServer::getInstance()->oscEvent, this, &OscUnit::onOSCMessage );
}

Json::Value OscUnit::save()
{
    Json::Value oscConfig = Json::Value( Json::objectValue );
    oscConfig["prefix"] = prefix;
    return oscConfig;
}

void OscUnit::load(Json::Value jsonData)
{
    if ( jsonData["prefix"] != Json::nullValue ) {
        strncpy(prefix, jsonData["prefix"].asCString(), 64);
    }
}


