#include "EffectUI.h"
#include "../Servers/MidiServer.h"

EffectUI::EffectUI() {
    
    enable = new UberToggle(" ##enable", &Tooltip::NONE);
    
    enable->addListener(this, &EffectUI::enableListener);
    enable->setSmall(true);
    enable->set(1.0f);
    
    parameters.push_back(enable);
    
}

EffectUI::~EffectUI() {
    for ( UberControl * p : parameters ) {
        delete p;
    }
    parameters.clear();
}

void EffectUI::parseLoadedParams() {
    Json::Value jsonAudioEffectsParams = *loadedParams;
    for ( unsigned int i = 0 ; i < parameters.size() ; i++ ) {
        UberControl * param = parameters[i];
        if ( jsonAudioEffectsParams[ param->getName() ] != Json::nullValue ) {
            param->set( jsonAudioEffectsParams[ param->getName() ].asFloat() );

            MidiServer::getInstance()->broadcastUberControlChange(param);
            OscServer::getInstance()->broadcastUberControlChange(param);
        } else {
            ofLog() << "WARNING: Parameter '" << param->getName() << "' not found for " << getName();
        }
    }
    
    delete loadedParams;
    loadedParams = nullptr;
}

Json::Value EffectUI::save() {
    Json::Value root = Json::Value( Json::objectValue );
    for ( unsigned int i = 0 ; i < parameters.size() ; i++ ) {
        UberControl * param = parameters[i];
        root[ param->getName() ] = param->get();
    }
    return root;
}
void EffectUI::load(Json::Value jsonAudioEffectsParams) {
    //I'll save this data and parseLoadedParams() later when I have the real ids.
    loadedParams = new Json::Value(jsonAudioEffectsParams);
}

void EffectUI::setIndex(int i, string indexPrefix) {
    string name;
    for ( unsigned int j = 0 ; j < parameters.size() ; j++ ) {
        name = parameters[j]->originalName + "##" + indexPrefix + "Effect" + ofToString(i);
        parameters[j]->setName(name);
    }
    
    //first time to set index I'll need to set the saved params
    if ( loadedParams != nullptr ) parseLoadedParams();
}

void EffectUI::drawGui() {
    for ( unsigned int i = 0 ; i < parameters.size() ; i++ ) {
        parameters[i]->draw();
    }
}

pdsp::Patchable &EffectUI::channelIn(size_t index)
{
    if ( index == 0 ) {
        return inL;
    } else if ( index == 1 ) {
        return inR;
    } else {
        throw getName() + " has stereo input only";
    }
}

pdsp::Patchable &EffectUI::channelOut(size_t index)
{
    if ( index == 0 ) {
        return outL;
    } else if ( index == 1 ) {
        return outR;
    } else {
        throw getName() + " has stereo input only";
    }
}

void EffectUI::enableListener(float &v) {
    
    if(v) {
        1.f >> switchL.in_select();
        1.f >> switchR.in_select();
    }
    else {
        0.f >> switchL.in_select();
        0.f >> switchR.in_select();
    }
}
