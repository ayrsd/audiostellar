#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "ofxImGui.h"

#include "./EffectUI.h"
#include "../GUI/UI.h"

class DimensionChorusUI : public EffectUI {
private:
    pdsp::Parameter speed;
    pdsp::Parameter depth;
    pdsp::Parameter delay;
    
    pdsp::DimensionChorus chorus;
    
public:
    DimensionChorusUI();

    string getName();

    pdsp::Patchable & channelIn(size_t index);
    pdsp::Patchable & channelOut(size_t index);
};
