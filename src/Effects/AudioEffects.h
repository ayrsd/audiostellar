#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "ofxImGui.h"
#include "ofxJSON.h"

#include "./EffectUI.h"

class EffectUI;

class AudioEffects : public pdsp::Patchable
{
private:
    vector<EffectUI *> effects; //should this be shared ?
    void patch();

    pdsp::PatchNode nodeIn0;
    pdsp::PatchNode nodeIn1;
    pdsp::PatchNode nodeOut0;
    pdsp::PatchNode nodeOut1;

    string indexPrefix;

public:
    AudioEffects();
    ~AudioEffects();

    void addEffect( EffectUI * newEffect );
    void removeEffect( EffectUI * effect );

    void removeMappings();

    Json::Value save();
    void load(Json::Value jsonAudioEffects);
    void setIndexesToEffects(string indexPrefix);

    void drawGui();
};
