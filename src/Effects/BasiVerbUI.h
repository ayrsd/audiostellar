#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "ofxImGui.h"

#include "./EffectUI.h"
#include "../GUI/UI.h"

#include "../GUI/UberSlider.h"

class BasiVerbUI : public EffectUI {
private:
    pdsp::Parameter rt60;
    pdsp::Parameter density;
    pdsp::Parameter damping;
//    pdsp::Parameter hicut; // this doesn't seem to be working

    pdsp::Parameter lfoFreq;
    pdsp::Parameter lfoAmount;

    pdsp::LinearCrossfader crossfader;
    pdsp::BasiVerb reverb;

    pdsp::ParameterAmp stereoAmpWet;
    pdsp::ParameterAmp stereoAmpDry;

    UberSlider * mix;
    UberSlider * rt;
    
    void onMix(float &v);
    void patch();
    bool patched = false;

    // This is a very nasty workaround
    // Basiverb randomly start with a loud noise
    // This way the loud noise does start but doesn't loop
    // indefinetly
    // Basiverb from ofxPDSP was also patched in our fork
    pdsp::ParameterGain bugfixGainOut;
    pdsp::ParameterGain bugfixGainBlackhole;
    int bugfixWait = 0;
    int bugfixFadeIn = 0;
    void update( ofEventArgs & args );

public:
    BasiVerbUI();
    ~BasiVerbUI();

    string getName();

    pdsp::Patchable & channelIn(size_t index);
    pdsp::Patchable & channelOut(size_t index);
};
