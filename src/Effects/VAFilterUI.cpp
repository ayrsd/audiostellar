#include "VAFilterUI.h"
#include "../GUI/UberSlider.h"

void VAFilterUI::parseLoadedParams()
{
    Json::Value jsonAudioEffectsParams = *loadedParams;
    if ( jsonAudioEffectsParams[ mode_current.getName() ] != Json::nullValue ) {
        mode_current = jsonAudioEffectsParams[ mode_current.getName() ].asInt();
        mode_values[mode_current] >> filter.in_mode();
    } else {
        ofLog() << "WARNING: Parameter '"<< mode_current.getName() <<"' not found for VAFilter";
    }

    EffectUI::parseLoadedParams();
}

VAFilterUI::VAFilterUI() {
    filter.channels(2);

    parameters.push_back( new UberSlider(cutoff.set("Cutoff",128.f,0.f,128.f),
                                         &Tooltip::NONE) );
    parameters.push_back( new UberSlider(resonance.set("Resonance",0.f,0.f,1.f),
                                         &Tooltip::NONE) );
    
    switchL.resize(2);
    switchR.resize(2);

    inL >> switchL.input(0);
    inR >> switchR.input(0);
    
    inL >> filter.ch(0) >> switchL.input(1);
    inR >> filter.ch(1) >> switchR.input(1);
    
    switchL >> outL;
    switchR >> outR;
    
    1.f >> switchL.in_select();
    1.f >> switchR.in_select();
    
    cutoff >> filter.in_cutoff();
    resonance >> filter.in_reso();
}

string VAFilterUI::getName()
{
    return "VAFilter";
}

Json::Value VAFilterUI::save()
{
    Json::Value root = EffectUI::save();
    root[mode_current.getName()] = mode_current.get();
    return root;
}

void VAFilterUI::setIndex(int i, string indexPrefix)
{
    string name = "Mode##" + indexPrefix + "Effect" + ofToString(i);
    mode_current.setName(name);

    EffectUI::setIndex(i, indexPrefix);
}

pdsp::Patchable &VAFilterUI::channelIn(size_t index)
{
    if ( index == 0 ) {
        return inL;
    } else if ( index == 1 ) {
        return inR;
    } else {
        throw "VAFilterUI has stereo input only";
    }
}

pdsp::Patchable &VAFilterUI::channelOut(size_t index)
{
    if ( index == 0 ) {
        return outL;
    } else if ( index == 1 ) {
        return outR;
    } else {
        throw "VAFilterUI has stereo output only";
    }
}

void VAFilterUI::drawGui()
{
    parameters[0]->draw(); //enable button
    
    if ( ImGui::Combo(mode_current.getName().c_str(),
         (int *)&mode_current.get(), mode_items, IM_ARRAYSIZE(mode_items)) ) {
        mode_values[mode_current] >> filter.in_mode();
    }

    for ( unsigned int i = 1 ; i < parameters.size() ; i++ ) {
        parameters[i]->draw();
    }
}
