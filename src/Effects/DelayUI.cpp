#include "DelayUI.h"
#include "../GUI/UberSlider.h"

DelayUI::DelayUI()
{
    delayL.setMaxTime(MAX_DELAY_TIME);
    delayR.setMaxTime(MAX_DELAY_TIME);

    parameters.push_back( new UberSlider(time.set("Time", 100.f, 1.f, MAX_DELAY_TIME), &Tooltip::NONE) );
    parameters.push_back( new UberSlider(feedback.set("Feedback", 0.2f, 0.f, 1.3f), &Tooltip::NONE) );
    parameters.push_back( new UberSlider(damping.set("Damping", 0.5f, 0.f, 0.99f), &Tooltip::NONE) );
    parameters.push_back( new UberSlider(mix.set("Mix", 0.5f, 0.f, 1.f), &Tooltip::NONE) );
    
    switchL.resize(2);
    switchR.resize(2);

    inL >> switchL.input(0);
    inR >> switchR.input(0);
    
    inL >> crossfaderL.in_A();
    inR >> crossfaderR.in_A();
    
    inL >> delayL >> crossfaderL.in_B();
    inR >> delayR >> crossfaderR.in_B();
    
    crossfaderL >> switchL.input(1);
    crossfaderR >> switchR.input(1);
    
    switchL >> outL;
    switchR >> outR;
    
    1.f >> switchL.in_select();
    1.f >> switchR.in_select();

    time >> delayL.in_time();
    time >> delayR.in_time();
    feedback >> delayL.in_feedback();
    feedback >> delayR.in_feedback();
    damping >> delayL.in_damping();
    damping >> delayR.in_damping();
    mix >> crossfaderL.in_fade();
    mix >> crossfaderR.in_fade();
    
}

string DelayUI::getName()
{
    return "Delay";
}

