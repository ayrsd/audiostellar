#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "AudioEngine.h"
#include "Sounds.h"
#include "ofxJSON.h"
#include "Voice.h"

class Voices {
public:
    Voices();
    ~Voices();

    enum VoicesType {
        Monophonic = 0,
        Polyphonic = 1,
        FullPolyphonic = 2
    };
    void setVoicesType( VoicesType type );
    VoicesType getVoicesType();
    void setChoke( bool c );
    bool getChoke();

    void update(ofEventArgs & args);

    void muteSound(shared_ptr<Sound> s);
    void enableEnvelope(bool e);
    void enableReverse(bool e);
    void setPitch(float p);
    void pitchBend(float delta);

    void setActive(bool v);

    void setVoicesAmount(int newVoiceAmount);
    int getVoicesAmount();

private:
    vector <Voice *> voices;
    map < shared_ptr<Sound>, vector <Voice *> > voiceMap;

    void die();

    VoicesType voiceType = FullPolyphonic;
    // if true and a sample is playing it will stop and play the new sample
    // only for mono and poly modes
    bool choke = true;

    int numVoices = 0;// 32; //default voices per unit
    int loadedVoices = 0;

    Voice * getVoice(shared_ptr<Sound> sound);
    Voice * getNonPlayingVoice(shared_ptr<Sound> sound);
    Voice * getFreeVoice();
    Voice * getReplaceableVoice();
    void takeOverVoice(shared_ptr<Sound> sound, Voice * voice);

    friend class Unit;
};

