#include "UberButton.h"

UberButton::UberButton(const string &name, const Tooltip::tooltip *tooltip)
    : UberControl (name, 0.f, 0.f, 1.f, tooltip)
{

}

UberButton::UberButton(ofParameter<float> &parameter, const Tooltip::tooltip *tooltip)
    : UberControl (parameter, tooltip)
{

}

UberButton::UberButton() : UberControl ()
{

}

void UberButton::draw(bool disabled)
{
    float tmpRef = get();
    bool needsUpdate = false;

    if ( style == Small ) {
        ImGui::SmallButton( ofxImGui::GetUniqueName(getName()) );
    } else {
        if ( style == Big ) {
            if ( !disabled ) {
                ImGui::PushStyleColor(ImGuiCol_Button, colors[GREEN][0]);
                ImGui::PushStyleColor(ImGuiCol_ButtonHovered, colors[GREEN][1]);
                ImGui::PushStyleColor(ImGuiCol_ButtonActive, colors[GREEN][2]);
            } else {
                ImGui::PushStyleColor(ImGuiCol_Button, colors[DISABLED][0]);
                ImGui::PushStyleColor(ImGuiCol_ButtonHovered, colors[DISABLED][1]);
                ImGui::PushStyleColor(ImGuiCol_ButtonActive, colors[DISABLED][2]);
                ImGui::PushStyleColor(ImGuiCol_Text, colors[DISABLED][3]);
            }
                ImGui::Button( ofxImGui::GetUniqueName(getName()), ImVec2(-1,40) );

            if ( !disabled ) {
                ImGui::PopStyleColor(3);
            } else {
                ImGui::PopStyleColor(4);
            }
        } else {
            ImGui::Button( ofxImGui::GetUniqueName(getName()), size );
        }
    }

    if ( ImGui::IsItemHovered() ) {
        if ( ImGui::IsMouseDown(0) && tmpRef == 0.f ) {
            tmpRef = 1.f;
            needsUpdate = true;
        } else if ( ImGui::IsMouseReleased(0) && tmpRef == 1.f ) {
            tmpRef = 0.f;
            needsUpdate = true;
        }
    }

    addUberFeatures(needsUpdate, tmpRef);
}
