#include "ColorPalette.h"
#include "../Utils/SessionManager.h"

ColorPalette* ColorPalette::instance = nullptr;


ColorPalette::ColorPalette() {
    
}


ColorPalette* ColorPalette::getInstance() {
    if ( instance == nullptr ) {
        instance = new ColorPalette();
    }

    return instance;
}

ofFloatColor ColorPalette::getColor(int index) {
    
    if ( index < 0 ) {
        return palettes[SessionManager::getInstance()->colorPalette][11]; //Last color in palette is noise color
    }
    
    return palettes[SessionManager::getInstance()->colorPalette][index % colorCount];
}

Json::Value ColorPalette::getDefaultPalette() {
    
    Json::Value palette = Json::objectValue;
    
    palette["name"] = DEFAULT_PALETTE;
    
    Json::Value colors = Json::arrayValue;
    for (int i = 0; i < defaultColors.size(); i++) {
        colors.append(defaultColors[i]);
    }
    
    palette["colors"] = colors;
    
    return palette;
    
}

void ColorPalette::newPalette() {
    
    vector<ofFloatColor> colors = vector<ofFloatColor>(colorCount + 1, ofFloatColor(1.f));
    
    palettes.insert( {"New_palette", colors} );
    
    SessionManager::getInstance()->colorPalette = "New_palette";
    renameMode(true);
}

void ColorPalette::duplicatePalette() {
    duplicatePalette(true);
}

void ColorPalette::duplicatePalette(bool enableRename) {
    
    vector<ofFloatColor> colors = palettes[SessionManager::getInstance()->colorPalette];
    string currentName = SessionManager::getInstance()->colorPalette;

    if ( !(currentName.find("_copy") == (currentName.size() - 5)) ) { //name does not end with "_copy"
        currentName = SessionManager::getInstance()->colorPalette + "_copy";
    }
    
    string newName = currentName;
    
    int n = 1;
    while (paletteExists(newName)) {
        newName = currentName + "_" + ofToString(n);
        n++;
    }
    
    palettes.insert( {newName, colors} );
    SessionManager::getInstance()->colorPalette = newName;
    renameMode(enableRename);
    
}

void ColorPalette::randomizePalette() {
    
    for ( auto& color : palettes[SessionManager::getInstance()->colorPalette]) {
        color.set(ofRandom(1.f), ofRandom(1.f), ofRandom(1.f));
    }
}


void ColorPalette::renameMode(bool renaming) {
    
    if (renaming) {
        for ( int i = 0 ; i < PALETTE_NAME_MAX_LENGTH ; i++ ) newPaletteName[i] = SessionManager::getInstance()->colorPalette[i];
        isRenamingPalette = true;
    }
    else {
        newPaletteName[0] = '\0';
        isRenamingPalette = false;
    }
    
    renameModeKeyboardFocus = true;
}

void ColorPalette::renamePalette() {
    
    auto i = palettes.find(SessionManager::getInstance()->colorPalette);
    if ( i != end(palettes) ) {
        auto value = std::move(i->second);
        palettes.erase(i);
        palettes.insert({newPaletteName, std::move(value)});
    }
    
    SessionManager::getInstance()->colorPalette = ColorPalette::getInstance()->newPaletteName;
    renameMode(false);
}

void ColorPalette::deletePalette(string name) {
    
    for (auto palette : palettes) {
        if (palette.first == name) {
            palettes.erase(palette.first);
            break;
        }
    }
        
    SessionManager::getInstance()->colorPalette = palettes.begin()->first;
}

void ColorPalette::load(Json::Value jsonData) {
    
    for (auto palette : jsonData) {
        
        vector<ofFloatColor> colors;
        for (int i = 0; i < palette["colors"].size(); i++) {
            colors.push_back( ofFloatColor::fromHex(palette["colors"][i].asInt()) );
        }
        
        palettes.insert( {palette["name"].asString(), colors} );
    }
}

Json::Value ColorPalette::save() {
    
    Json::Value palettes = Json::arrayValue;
    
    for (auto p : this->palettes) {
        
        Json::Value palette = Json::objectValue;
        Json::Value colors = Json::arrayValue;
        
        for(auto color : p.second) {
            colors.append(color.getHex());
        }
        
        palette["name"] = p.first;
        palette["colors"] = colors;
            
        palettes.append(palette);

      }
    
    return palettes;
}

void ColorPalette::revert() {
    
    revert(SessionManager::getInstance()->colorPalette);
}

void ColorPalette::revert(string name) {
    
    const ofxJSONElement jsonData = SessionManager::getInstance()->getSettingsFile()["colorPalettes"];
    
    for (auto palette : jsonData) {
        
        if (palette["name"] == name) {
            
            vector<ofFloatColor> colors;
            for (int i = 0; i < palette["colors"].size(); i++) {
                colors.push_back( ofFloatColor::fromHex(palette["colors"][i].asInt()) );
            }
            
            palettes[name] = colors;
        }
    }
}


bool ColorPalette::paletteExists(string name) {
    return palettes.count(name);
}

unordered_map<string, vector<ofFloatColor>>& ColorPalette::getPalettes() {
    return palettes;
}
