#pragma once

#include "ofMain.h"
#include "UberControl.h"
#include "ofxImGui.h"
/**
 * @brief The UberButton class
 *
 * This is the button version of uberControl
 */
class UberToggle : virtual public UberControl
{
public:
    UberToggle( const std::string& name, const Tooltip::tooltip * tooltip, const float& v = 0.f );
    UberToggle( ofParameter<float>& parameter, const Tooltip::tooltip * tooltip );
    UberToggle();

    const ImVec4 COLOR_ACTIVE = ImVec4(.0f, .67f, .42f, 1.f);
    const ImVec4 COLOR_SOLO = ImVec4(0.919f, 0.814f, 0.240f, 1.0f);
    const ImVec4 COLOR_MUTE = ImVec4(1.0f, 0.f, 0.f, 1.0f);
    const ImVec4 COLOR_DEFAULT = ImGui::GetStyle().Colors[ImGuiCol_ButtonActive];

    enum Style {
        BUTTON = 0,
        CHECKBOX = 1
    };

    void setActive(bool active = true);
    bool isActive();
    void setStyle(Style s);
    Style getStyle();
    void setSmall(bool s);
    bool getSmall();
    void setColor(ImVec4 c);
    ImVec4 getColor();

    void draw();
private:
    ImVec4 color = COLOR_DEFAULT;
    Style style = BUTTON;
    bool small = false;
};
