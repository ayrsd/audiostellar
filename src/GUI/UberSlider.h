#pragma once

#include "ofMain.h"
#include "UberControl.h"
/**
 * @brief The UberSlider class
 *
 * This is the slider version of uberControl
 */
class UberSlider : virtual public UberControl
{
public:
    UberSlider( const std::string& name, const float& v, const float& min, const float& max, const Tooltip::tooltip * tooltip );
    UberSlider( ofParameter<float>& parameter, const Tooltip::tooltip * tooltip );
    UberSlider();

    //Normal format
    string format = "%.2f";
    //Label when max is reached
    string maxFormat;
    //Label when min is reached
    string minFormat;

    //Lineal or log ? (default lineal)
    bool log = false;
    //Small slider? Like the pan control
    bool small = false;

    void draw();
};
