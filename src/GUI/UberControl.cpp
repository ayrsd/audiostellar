#include "UberControl.h"
#include "../Servers/OscServer.h"
#include "../Servers/MidiServer.h"
#include "../Utils/SessionManager.h"

UberControl * UberControl::selectedControl = nullptr;
ofEvent<UberControl> UberControl::uberControlIsDead = ofEvent<UberControl>();

void UberControl::checkUnknownMappings()
{
    // when loading a session osc and midi server will have
    // only labels, not the float reference
    if ( OscServer::getInstance()->hasUnknownMappings() ) {
        if ( OscServer::getInstance()->fixUnknownMapping(this) ) {
            OscServer::getInstance()->broadcastUberControlChange(this);
            hasOSCMapping = true;
        }
    }
    if ( MidiServer::getInstance()->hasUnknownMappings() ) {
        vector<MIDIMapping> mappings = MidiServer::getInstance()->fixUnknownMapping(this);
        for ( auto &m : mappings ) {
            if ( !m.useMIDIKeyboard ) {
                hasMIDIMapping = true;
            } else {
                usesMIDIKeyboard = true;
            }
        }
        if ( mappings.size() ) {
            // Avoid double broadcasting on load.
            // Effects broadcast again later, when they load its settings
            if ( this->getName().find("Effect") == string::npos ) {
                MidiServer::getInstance()->broadcastUberControlChange(this);
            }
        }
    }
}

UberControl::UberControl(const std::string& name, const float& v, const float& min, const float& max, const Tooltip::tooltip *tooltip)
    : ofParameter<float> (name, v, min, max)
{
    setDefaultValue(v);
    originalName = name;
    this->tooltip = tooltip;

}

UberControl::UberControl(ofParameter<float> &parameter, const Tooltip::tooltip *tooltip) : ofParameter<float>(parameter)
{
    setDefaultValue( parameter.get() );
    originalName = parameter.getName();
    this->tooltip = tooltip;
}

UberControl::UberControl() : ofParameter<float> ()
{

}

UberControl::~UberControl()
{
    // this isn't really useful since when units are removed, they are not
    // REALLY removed. I leave this here anyway for future use
//    MidiServer::getInstance()->removeMapping(this);
//    ofNotifyEvent(UberControl::uberControlIsDead, *this);
}

void UberControl::setName(const string &name)
{
    ofParameter<float>::setName(name);
    if ( originalName.empty() ) {
        originalName = name;
    }

    checkUnknownMappings();
}

string UberControl::getName() const
{
    return ofParameter<float>::getName();
}

UberControl &UberControl::set(float v)
{
    ofParameter<float>::set(v);
    return *this;
}

const float &UberControl::get() const
{
    return ofParameter<float>::get();
}

float UberControl::getMin() const
{
    return ofParameter<float>::getMin();
}

float UberControl::getMax() const
{
    return ofParameter<float>::getMax();
}

void UberControl::setDefaultValue(float v)
{
    defaultValue = v;
    defaultValueSet = true;
}

float UberControl::getDefaultValue()
{
    return defaultValue;
}

void UberControl::setMin(float v)
{
    ofParameter<float>::setMin(v);
}

void UberControl::setMax(float v)
{
    ofParameter<float>::setMax(v);
}

void UberControl::setTooltip(const Tooltip::tooltip * tooltip)
{
    this->tooltip = tooltip;
}

void UberControl::removeAllMappings()
{
    MidiServer::getInstance()->removeMapping(this);
    MidiServer::getInstance()->disableMIDIKeyboard(this);
    OscServer::getInstance()->removeMapping(this);
}

void UberControl::setPalette(Palette p)
{
    currentPalette = p;
}

void UberControl::clickOutsidePopup()
{
    selectedControl = nullptr;
}

ofParameter<float> *UberControl::getParameter()
{
    return this;
}

void UberControl::addUberFeatures(bool activated, const float &tmpRef)
{
    if ( activated ) {
        if ( defaultValueSet && ofGetKeyPressed(OF_KEY_SHIFT) ) {
            set(defaultValue);
        } else {
            set(tmpRef);
        }

        OscServer::getInstance()->broadcastUberControlChange(this);
        MidiServer::getInstance()->broadcastUberControlChange(this);
    }

    checkUnknownMappings();

    if(ImGui::IsItemHovered() && this->tooltip != nullptr) Tooltip::setTooltip(*this->tooltip);

    if ( ImGui::IsItemHovered() &&
         ImGui::IsMouseReleased(1) &&
         selectedControl == nullptr &&
         !MidiServer::getInstance()->getMIDILearn()) {
         selectedControl = this;
    }
    if ( selectedControl == this ) {
        ImGui::OpenPopup("controlOptions");
        if (ImGui::BeginPopup("controlOptions")) {
            if ( !hasMIDIMapping ) {
                if (ImGui::MenuItem("MIDI Learn")) {
                    if ( MidiServer::getInstance()->isAnyDeviceConnected() ) {
                        MidiServer::getInstance()->setMIDILearnOn( this );
                    } else {
                        Gui::getInstance()->showMessage("No MIDI device is configured. \n\n"
                                                        "Use the Settings menu.", "Error");
                    }
                    selectedControl = nullptr;
                    ImGui::CloseCurrentPopup();
                }
            } else {
                if (ImGui::MenuItem("Remove MIDI mapping")) {
                    MidiServer::getInstance()->removeMapping( this );
                    selectedControl = nullptr;
                    hasMIDIMapping = false;
                    ImGui::CloseCurrentPopup();
                }
            }

            // This needs a little more work but I don't think is an useful feature right now.
            // There is a problem with the indexes, if All is selected

//            if ( ImGui::BeginMenu("Use MIDI Keyboard") ) {
//                for ( unsigned int i = 0 ; i < useMIDIKeyboardChannels.size() ; i++ ) {
//                    string c = useMIDIKeyboardChannels[i];
//                    int channel = getMIDIKeyboardChannel();
//                    bool selected = false;

//                    if ( channel == -1 && c == "None" ) {
//                        selected = true;
//                    } else if ( channel == 0 && c == "All" ) {
//                        selected = true;
//                    } else if ( ofToString( channel + 2 ) == c ) {
//                        selected = true;
//                    }

//                    if (ImGui::MenuItem(c.c_str(), NULL, selected )) {
//                        if ( MidiServer::getInstance()->isAnyDeviceConnected() ) {
//                            MidiServer::getInstance()->disableMIDIKeyboard( this );
//                            if ( c != "None" ) {
//                                if ( c == "All" ) {
//                                    MIDIKeyboardChannel = 0;
//                                } else {
//                                    MIDIKeyboardChannel = i - 2;
//                                }
//                                MidiServer::getInstance()->enableMIDIKeyboard( this, MIDIKeyboardChannel );
//                            } else {
//                                MIDIKeyboardChannel = -1;
//                            }
//                        } else {
//                            Gui::getInstance()->showMessage("No MIDI device is configured. \n\n"
//                                                            "Use the Settings menu.", "Error");
//                        }
//                        selectedControl = nullptr;
//                        ImGui::CloseCurrentPopup();
//                    }
//                }
//                ImGui::EndMenu();
//            }

            if ( !hasOSCMapping ) {
                if (ImGui::MenuItem("OSC Learn")) {
                    OscServer::getInstance()->setOSCLearnOn( this );
                    selectedControl = nullptr;
                    ImGui::CloseCurrentPopup();
                }
            } else {
                if (ImGui::MenuItem("Remove OSC mapping")) {
                    OscServer::getInstance()->removeMapping( this );
                    selectedControl = nullptr;
                    hasOSCMapping = false;
                    ImGui::CloseCurrentPopup();
                }
            }

            ImGui::EndPopup();
        }
    }
}

int UberControl::getMIDIKeyboardChannel()
{
    if ( MIDIKeyboardChannel == -2 ) {
        MIDIKeyboardChannel = MidiServer::getInstance()->getMIDIKeyboardChannel(this);
    }

    return MIDIKeyboardChannel;
}

void UberControl::setMIDIKeyboardChannel(int newMIDIKeyboardChannel)
{
    MIDIKeyboardChannel = newMIDIKeyboardChannel;
}
