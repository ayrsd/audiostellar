////
/// Based on https://gitlab.com/f00b455/audiostellar
///
#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxImGui.h"
#include "ofxJSON.h"

#include "../Utils/Tooltip.h"
#include "../GUI/UberControl.h"
#include "../Sound/Sound.h"
#include "ofxAbletonLink.h"

class AbletonLinkServer
{

private:
    AbletonLinkServer();
    static AbletonLinkServer *instance;
    ofxAbletonLink abletonLink;


    void bpmChanged(double &bpm);
    void playStateChanged(bool &b);

public:

    static AbletonLinkServer *getInstance();
    static bool enable;

    bool useExternalPlayStop = false;
    bool playState = true;
    float latency = 0;
    int fire = 1;
    int tickValue = 1;
    void update();

    // Only MasterClock should send false
    void start(bool setLinkStatus = true);
    void stop(bool setLinkStatus = true);

    void drawGui();

    void bpmListener(float &v);

    void reset();

    int getBPM();
    void setBPM(int v);

    Json::Value save();
    void load(Json::Value jsonData);
};
