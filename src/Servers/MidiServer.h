#pragma once

#include "ofMain.h"

#include "ofxImGui.h"
#include "ofxMidi.h"
#include "ofxMidiClock.h"
#include "ofxJSON.h"

#include "../Utils/MIDIMessage.h"
#include "../Units/Units.h"
#include "./MasterClock.h"
#include "../GUI/UberControl.h"
#include "../GUI/UberButton.h"

#define MIDI_CHANNELS 16

class Units;

struct MIDIMapping {
    int channel = -1;
    int cc = -1;
    int pitch = -1;

    bool usesVelocity = false;
    bool ignoreNoteOff = false;
    float range[2] = {0.f,1.f};

    bool useMIDIKeyboard = false;

    UberControl * parameter = nullptr;
    string label; //for saving to file
};

enum ControllerTemplateParameterLabels {
    INVALID,
    NONE,

    VOLUME,
    PAN,

    MUTE,
    SOLO,
    ACTIVE,
    SELECTED,

    PITCH,

    CHANNEL,
    MASTER_VOLUME,
    TEMPO,

    MUTE_ALL,
    SOLO_ALL,
    ACTIVE_ALL,

    CAM_X,
    CAM_Y,
    CAM_ZOOM_IN_FAST,
    CAM_ZOOM_OUT_FAST,
    CAM_ZOOM_IN,
    CAM_ZOOM_OUT,
    CAM_ZOOM
};

struct MIDIMappingControllerTemplate {
    int channel = -1;
    int cc = -1;
    int pitch = -1;

    int dead_zone = -1; // only CAM_X CAM_Y
    bool invert = false; //only cc

    int unitIndex = -1;
    ControllerTemplateParameterLabels parameterLabel = NONE;
};

struct MIDIKeyboardNotes {
    vector<float> velocities;
    int channel;
    int status;
};

class MidiServer: public ofxMidiListener {
private:
    MidiServer();
    static MidiServer* instance;

    map<string, ofxMidiIn *> midiDevicesIN;
    map<string, ofxMidiOut *> midiDevicesOUT;
    unsigned int lastCheckForMidiDevices = 0;

    ofxMidiIn midiIn;
    ofxMidiOut midiOut;
    ofxMidiClock clock;

    int currentBeat = -1;

    enum SendMidiType {
        NOTE_ON,
        CONTROL_CHANGE
    };
    struct SendMidiMessage {
        SendMidiType type = NOTE_ON;
        int channel = -1;
        int id = -1;
        int value = -1;
        string strMIDIDevice = "";
    };
    queue<SendMidiMessage> midiMessagesToSend;
    int SEND_MIDI_MAX_MESSAGES_PER_UPDATE = 20;
//    const int SEND_MIDI_TIME = 1; //ms
//    uint16_t lastMidiSent = 0;
    void sendQueuedMidiMessages();
    void sendMidiMessage( SendMidiType type,
                          int channel,
                          int id,
                          int value,
                          string strMIDIDevice = "");

    void checkForNewMidiDevices();
    void addMidiDeviceIn(string name);
    void removeMidiDeviceIn(string name);
    void addMidiDeviceOut(string name);
    void removeMidiDeviceOut(string name);
    void newMidiMessage(ofxMidiMessage& eventArgs);
    void checkPolyNoteOn();
    bool checkControllerMapping(const MIDIMessage& msg);
    UberControl * getParameterFromControllerTemplate(const MIDIMappingControllerTemplate &t);
    void broadcastAllMidiMappings(string strMIDIDevice = "");

    MIDIMapping * lastControlMoved = nullptr;
    vector<MIDIMapping> midiMappings;
    bool unknownMappings = false;
    vector<MIDIMappingControllerTemplate> controllerTemplate;

    ofVec3f moveCamDelta = {0,0,0};
    float moveCamZoom = 0.f;
    float moveCamZoomAbs = 0.f;
    void moveCam();

    MIDIKeyboardNotes midiKeyboardNotes[MIDI_CHANNELS];
    uint64_t lastMIDIKeyboardNoteMillis[MIDI_CHANNELS] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    int midiKeyboardNotesWaitTime = 10;

    bool arrPolyNoteOn[128];//TODO remove

    bool polyNoteOn = false;

    bool midiLearn = false;
    bool midiLearnUsesVelocity = false;
    bool midiLearnIgnoreNoteOff = false;
    float midiLearnRange[2] = {0.f,1.f};

    const ImVec4 MONITOR_COLOR_ACTIVE = ImVec4(0.919f, 0.814f, 0.240f, 1.0f);
    const ImVec4 MONITOR_COLOR_INACTIVE = ImVec4(0.2,0.2,0.2,1);
    UberButton btnMonitor{"MIDI##Monitor", &Tooltip::MIDI_MONITOR};
    uint64_t lastMIDIMessageMillis = 0;

    string controllerTemplatePath = "";
public:
    static MidiServer* getInstance();
    ~MidiServer() {}

    static ofEvent<MIDIKeyboardNotes> midiKeyboardEvent;
    
    bool useMIDIClock = false;

    //For handling play/stop from external device
    bool midiClockPlaying = true;
    bool handleMIDIPlayStop = false;

    void setMIDILearnOn(UberControl * parameter);
    void setMIDILearnOff();
    bool getMIDILearn();
    // when loads from file it will be true
    // then uberslider will take care of this
    bool hasUnknownMappings();
    //if label is in mappings array it will set it
    vector<MIDIMapping> fixUnknownMapping(UberControl *parameter);
    void removeMapping( UberControl *parameter);
    void broadcastUberControlChange(UberControl * parameter);
    void onSelectedUnit(int index);

    void enableMIDIKeyboard(UberControl * parameter, int channel = -1);
    void disableMIDIKeyboard(UberControl * parameter);
    int getMIDIKeyboardChannel(UberControl * parameter);

    Json::Value save();
    void load(Json::Value jsonData);

    void drawSettings();
    void drawMonitor();
    void drawMIDILearn();
    void update();

    void reset();

    void onUberControlDead( UberControl & deadControl );

    bool isAnyDeviceConnected();
    vector<string> getConnectedInputDevices();
    vector<string> getConnectedOutputDevices();
    void connectDevicesIn(const vector<string> & devices);
    void connectDevicesOut(const vector<string> & devices);

    bool loadControllerTemplateFile(string path);
    void clearControllerTemplateFile();
    bool isControllerTemplateFileEnabled();
    ControllerTemplateParameterLabels controllerConfigToLabel(string config);
    const string & getControllerTemplatePath();
};
