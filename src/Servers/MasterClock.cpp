#include "MasterClock.h"
#include "./AbletonLinkServer.h"

MasterClock* MasterClock::instance = nullptr;

MasterClock::MasterClock()
{
    bpm.addListener(this, &MasterClock::bpmListener);
    bpm.addListener(AbletonLinkServer::getInstance(), &AbletonLinkServer::bpmListener);
    bpm.format = "%.0f";

    btnLink.setPalette(UberControl::YELLOW);
    btnLink.addListener(this, &MasterClock::onLinkClick);

    initPDSPFunctionClock();
}

MasterClock *MasterClock::getInstance()
{
    if ( instance == nullptr ) {
        instance = new MasterClock();
    }
    return instance;
}

void MasterClock::setClockType(ClockType t)
{
    if ( t == MIDI && !MidiServer::getInstance()->isAnyDeviceConnected() ) {
        return;
    }
    currentClockType = t;
    if ( currentClockType == MIDI || currentClockType == ABLETON_LINK ) {
        destroyPDSPFunctionClock();
    } else {
        initPDSPFunctionClock();
    }
}

MasterClock::ClockType MasterClock::getClockType()
{
    return currentClockType;
}

void MasterClock::onMIDIClockTick(MIDIMessage m)
{
    if (getClockType() != MIDI) return;
    if (!playing) return;
    // MIDI clock sends every 32th and we are using 16th
//    if ( m.beats % 2 == 0 ) {
//        int realbeat = m.beats / 2;
//        ofNotifyEvent(onTempo, realbeat);
//    }
    // Back to 16th
    onTempo.notify(this, m.beats );
//    ofNotifyEvent(onTempo, m.beats);
    
    
    //estimate MIDI clock BPM
    //only used to determine sequence cursor position
    //average coud be calculated but seems ok like this
    uint64_t currentMidiClockTickTime = ofGetElapsedTimeMillis();
    float interval = ( currentMidiClockTickTime - lastMidiClockTickTime) / 1000.f ;
    lastMidiClockTickTime = currentMidiClockTickTime;
    float BPM = 60.f / (interval * 4);
    setBPM(BPM);
    
}

void MasterClock::onAbletonLinkClockTick(int p)
{
    onTempo.notify(this, p);
//    ofNotifyEvent(onTempo, p);
}

void MasterClock::onLinkClick(float &v)
{
    if ( v == 1 ) {
        AbletonLinkServer::getInstance()->start(false);
    } else {
        AbletonLinkServer::getInstance()->stop(false);
    }
}

void MasterClock::setLinkStatus(bool b)
{
    btnLink.set(b ? 1.0f : 0);
}

void MasterClock::initPDSPFunctionClock()
{
    pdspFunction = new pdsp::Function();
//    pdspFunction->timing = 16;
    // this assignable function is executed each 16th
    pdspFunction->code = [&]() noexcept {
        int frame = pdspFunction->frame();
        if ( playing ) {
            onTempo.notify(this, frame );
//            ofNotifyEvent(onTempo, frame);
        }
    };
}

void MasterClock::destroyPDSPFunctionClock()
{
    if ( pdspFunction != nullptr ) {
        delete pdspFunction;
        pdspFunction = nullptr;
    }
}

void MasterClock::drawGui()
{
    if ( getClockType() != MIDI ) {
        btnLink.draw();
        ImGui::SameLine();
//        ImGui::Dummy(ImVec2(1,0));
//        ImGui::SameLine();
        ImGui::PushItemWidth( 100.f );
        bpm.draw();


    } else {
        ImGui::Button("MIDI Clock");
    }
}

void MasterClock::play()
{
    playing = true;
}

void MasterClock::stop()
{
    playing = false;
}

bool MasterClock::isPlaying()
{
    return playing;
}

float MasterClock::getBPM()
{
    return bpm;
}

void MasterClock::setBPM( float theBPM )
{
    bpm.set(theBPM);
}

void MasterClock::bpmListener(float &v) {
    AudioEngine::getInstance()->setTempo(round(v));
}
