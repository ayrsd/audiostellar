#!/bin/bash
PROJECT_DIR=$(realpath "$0")
PROJECT_DIR=$(dirname "$PROJECT_DIR")
OS=$(uname -s)

if [ -z "${OF_ROOT}" ]; then
    echo "OF_ROOT enviroment variable is undefined."

    read -p "Is your app in apps/myApps folder? " -n 1 -r
    echo    # (optional) move to a new line
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        OF_ROOT=$PROJECT_DIR/../../..
    else
        echo "Rerun this script setting OF_ROOT enviroment variable: $ export OF_ROOT=<path/to/of>\n "
        exit
    fi
fi

mkdir -p $PROJECT_DIR/bin/data 
if [ ! -L "$PROJECT_DIR/bin/data/assets" ]; then
    ln -s ../../assets/ $PROJECT_DIR/bin/data/assets
fi

#### OF customizations

if [ "$OS" == "Darwin" ]; then
    #sed -i '' '1608s/.*/if(instance->events().notifyExit()) glfwSetWindowShouldClose(windowP_, false);/' $OF_ROOT/libs/openFrameworks/app/ofAppGLFWWindow.cpp
    
    #exit message, fullscreen fix
    patch $OF_ROOT/libs/openFrameworks/app/ofAppGLFWWindow.cpp ./patches/macOS-OF-ofAppGLFWWindow.cpp.patch
else
    #0.12.0
    sed -i '1635s/.*/if(instance->events().notifyExit()) glfwSetWindowShouldClose(windowP_, false);/' $OF_ROOT/libs/openFrameworks/app/ofAppGLFWWindow.cpp
    #0.11.2
    #sed -i '1616s/.*/if(instance->events().notifyExit()) glfwSetWindowShouldClose(windowP_, false);/' $OF_ROOT/libs/openFrameworks/app/ofAppGLFWWindow.cpp
fi

# Linux icon | No longer needed in of 0.12.0
#if [ "$OS" == "Linux" ]; then
#    sed -i '157s/.*/XIC	getX11XIC(); void setWindowIcon(const std::string \& path); void setWindowIcon(const ofPixels \& iconPixels);/' $OF_ROOT/libs/openFrameworks/app/ofAppGLFWWindow.h
#    sed -i '197s/.*//' $OF_ROOT/libs/openFrameworks/app/ofAppGLFWWindow.h
#    sed -i '198s/.*//' $OF_ROOT/libs/openFrameworks/app/ofAppGLFWWindow.h
#fi
####


git clone https://github.com/danomatika/ofxMidi $OF_ROOT/addons/ofxMidi
git -C $OF_ROOT/addons/ofxMidi checkout f9d85fd888ba23cf49207b362e2bcc8cfad352ed

git clone https://github.com/genekogan/ofxConvexHull $OF_ROOT/addons/ofxConvexHull
git -C $OF_ROOT/addons/ofxConvexHull checkout 58a1a403e873189c9fb88d9b44988b11bc580821

git clone https://github.com/macramole/ofxAudioFile $OF_ROOT/addons/ofxAudioFile
git clone https://github.com/macramole/ofxJSON $OF_ROOT/addons/ofxJSON
git clone https://github.com/macramole/ofxImGui $OF_ROOT/addons/ofxImGui
git clone https://github.com/macramole/ofxPDSP $OF_ROOT/addons/ofxPDSP
git clone https://github.com/macramole/ofxAnn $OF_ROOT/addons/ofxAnn

git clone --recursive https://github.com/2bbb/ofxAbletonLink.git $OF_ROOT/addons/ofxAbletonLink

echo "DONE"
